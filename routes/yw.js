/**
 * @author Morgan Conrad
 * Copyright(c) 2013
 */

var RRContext = require('../lib/rrcontext');

exports.ywRedirect = function (yahoo) {
    "use strict";
    return function (req, res) {
        var rrContext = new RRContext(req, res, 'akc/list');
        var location = req.params.location;

        if (!location) {
            rrContext.sendError('Please provide a location', 404);
            return;
        }

        yahoo.getPlaceInfo(location, function (error, placeInfo) {
            if (error)
                rrContext.sendError(error, 500);
            else if (!placeInfo)
                rrContext.sendError('Cannot find location: ' + location, 404);
            else
                res.redirect(302, 'http://weather.yahoo.com/' + placeInfo.path + '-' + placeInfo.woeid);
        });

    };
};