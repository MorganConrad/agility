
var RRContext = require('../lib/rrcontext');
var Utils = require('../lib/utils');
var Collector = require('../lib/collector');
var Show = require('../lib/show');
var QueryString = require('querystring');


var orgHelpers = {
    akc: require('../orgs/AKC'),
    cpe: require('../orgs/CPE'),
    nadac: require('../orgs/NADAC'),
    uki: require('../orgs/UKI'),
    usdaa: require('../orgs/USDAA'),
    fun: require('../orgs/FUN'),
    work: require('../orgs/WORK')
};


function queryAndReturn(rrContext, orgs, states, miles, collector) {
    "use strict";
    
    var query = global.nextq.mdb.buildQuery(rrContext.req, states);
    //if (miles) {  // this was really slow...
       // query.longLat = { $near: collector.results.placeinfo.longLat, $maxDistance: 1609*miles };
    //}

   global.nextq.mdb.loadCollections(orgs, query, { sort: {_id: 1}}, function (err, results) {
        if (err) {
           global.nextq.config.log.error(err);  // TODO return to user too
            rrContext.sendError(err, 500);
        }
        else {
            var allShows = [];
            orgs.forEach(function (org) {
                allShows = allShows.concat(results[org]);
            });
            
            // the mongo geo query is weirdly slow, do myself
            if (+miles && rrContext.longLat[1] < 100.0) {  // + forces to number
                var lon = rrContext.longLat[0];
                var lat = rrContext.longLat[1];
                allShows = allShows.filter(function(show) {
                    return Show.distanceFrom(lat, lon, show) <= miles;
                });
            }
            
            if (orgs.length > 1)
                allShows.sort(function(a,b) {
                    if (a._id < b._id)
                       return -1;
                    else if (a._id > b._id)
                        return 1;
                    return 0;
                });
            
            rrContext.send('shows', allShows); 
           
           // see if need to save stuff
           global.nextq.mdb.saveStuff();
        }
    }, this);
}


exports.search = function(req, res){
    "use strict";
    // var rrContext = new RRContext(req, res, 'akc/list');
    var rrContext = new RRContext(req, res, 'showlist');
    var states = Utils.trimStates(req.query['states']);
    var orgs = Utils.toArray(req.query['orgs']);
    var near = req.query['near'];    
    var miles = req.query['miles'];
    
   // in theory these get caught by the GUI but just in case...
    if (!states.length){
       rrContext.sendError('You must select a state', 400);
       return;
    }
   if (!orgs.length) {
      rrContext.sendError('You must select a club', 400);
      return;
   }
   rrContext.mobile = req.query['mob'];
   
    var calls = orgs.length;
    if (near) ++calls;
    
    var myCollector = new Collector(calls, function(errors, results) {
        if (errors) {
            global.nextq.config.log.error(errors);
            rrContext.sendError(errors, 500);
        }
        else queryAndReturn(rrContext, orgs, states, miles, myCollector);
    });
    
    orgs.forEach(function(org) {
       orgHelpers[org].bringUpToDate(rrContext, states, function(err) {
           myCollector.collect(err, org);
       }); 
    });
    
    if (near) {
       global.nextq.geocoder.getPlaceInfo(near, function(err, placeinfo){
            if (placeinfo) {
                rrContext.setLongLat(placeinfo.longLat); 
                rrContext.near = near;
            }
            myCollector.collect(err, 'placeinfo', placeinfo);
        });
    }
      
};


exports.search1 = function(req, res){
   "use strict";
   var showID = QueryString.unescape(req.params.id);
   var rrContext = new RRContext(req, res, 'expand');
   var orgKey = Show.org(showID);

   global.nextq.mdb.findOneShow(showID, function(err, results) {
      if (err)
         global.nextq.config.log.error(err);  // TODO return to user too
      else {
         var shows = results[orgKey];
         if (shows.length) {
            var show = shows[0];
            show.mapurl = 'http://maps.googleapis.com/maps/api/staticmap?center=' +
                          show.longLat[1] + ',' + show.longLat[0] + '&zoom=11&size=400x400&sensor=false';
            
            rrContext.send('shows', shows);
         }
         else
            rrContext.sendError('cannot find showid: ' + showID, 404);
      }
   }, this);
};


function _searchByIds(req, res, showIDs, keepOrder) {
   "use strict";
   if (!Array.isArray(showIDs))
      showIDs = showIDs.split('+');
   var near = req.query['near'];
   var rrContext = new RRContext(req, res, 'showlist');  // need another template
   
   if (showIDs.length === 0 || showIDs[0].length === 0) {
      rrContext.sendError('you must select at least one show under the checkbox column.', 400);
      return;
   }

   if (near) {
      global.nextq.geocoder.getPlaceInfo(near, function(err, placeinfo){
         if (placeinfo) {
            rrContext.setLongLat(placeinfo.longLat);
            rrContext.near = near;
         }
      });
   }

   global.nextq.mdb.findByIDs(showIDs, function(err, results) {
      if (err)
         global.nextq.config.log.error(err);  // TODO return to user too
      else {
         var allShows = [];
         for (var key in results)
            allShows = allShows.concat(results[key]);
         // to do order shows
         if (allShows.length) {
            if (keepOrder) {
               var orderedShows = [];
               for (var i=0; i<allShows.length; i++) {
                  var idx = showIDs.indexOf(allShows[i]._id);
                  orderedShows[idx] = allShows[i];
               }
               allShows = orderedShows;
            }
            rrContext.send('shows', allShows);
         }
         else
            rrContext.sendError('cannot find showids: ' + showIDs, 400);
      }
   }, this);
}


exports.searchByIDsInParams = function(req, res){
   "use strict";
   var showIDs = QueryString.unescape(req.params.ids);
   _searchByIds(req, res, showIDs);
};


exports.searchByIDsInQuery = function(req, res){
   "use strict";
   var showIDs = req.query.snap;
   _searchByIds(req, res, showIDs);
};




