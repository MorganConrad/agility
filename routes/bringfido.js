

const BF_ROOT = 'http://www.bringfido.com';
const BF_PATH = '/lodging/city/';
const BF_AFFIL = '/track?aid=NEXTQ';

var Request = require('request');
var RRContext = require('../lib/rrcontext');

exports.bfRedirect = function (req, res) {
   "use strict";
   var rrContext = new RRContext(req, res, 'akc/list');
   var location = req.params.location;

   if (!location) {
      rrContext.sendError('Please provide a location', 404);
      return;
   }
   
   var direct = req.query.directBF;

   if (direct) {
      // BringFido uses _ instead of spaces and dislikes commas
      var bfLocation = location.replace(/,/g, '');
      bfLocation = bfLocation.replace(/ /g, '_');
      // TODO be smarter about country - maybe use the geocoder eventually...
      // Sept 2016 BringFido wants lowercase in bfLocation
      var url = BF_ROOT + BF_PATH + bfLocation.toLowerCase() + '_us' + BF_AFFIL;

      Request.head(url, function (error, response, body) {
         if (error || (response.statusCode >= 400))  // small city, they don't have a page for it
            res.redirect(302, BF_ROOT + BF_AFFIL);   // redirect to generic page
         else
            res.redirect(302, url);
      });
   }
   
   else {
      res.render("gobf", { bfrdURl : req.url + '?directBF=true'} );
   }

};