/**
 * @author Morgan Conrad
 * Copyright(c) 2014
 */

var RRContext = require('../lib/rrcontext');
var utils = require('../lib/utils');
var Show = require('../lib/show');


// NOTE: all these should "really" be POSTS, not GETS, but whatever, a GET is much easier to trigger


function resetMongodb(rrContext, more) {
    "use strict";
   global.nextq.mdb.reset(more, function(err) {
        if (err)
            rrContext.sendError(err, 500);
        else
           rrContext.send('admin', 'resetMongodb successful');
    });
}

function purgeOld(rrContext, date) {
    "use strict";
    date = date || new Date();
   global.nextq.mdb.purgeOld(date, function(err) {
        if (err)
            rrContext.sendError(err, 500);
        else
            rrContext.send('admin', 'purgeOld successful for date: ' + date.toDateString());
    });
}

function addAuthor(rrContext) {
   "use strict";
   var newAuthor = { _id: rrContext.req.query.id, author: rrContext.req.query.author};
   global.nextq.mdb.insert('authors', newAuthor, function(err) {
      if (err)
         rrContext.sendError(err, 500);
      else
         rrContext.send('admin', 'addAuthor successful');
   });
}



function saveStuff (rrContext) {
    "use strict";
   global.nextq.mdb.saveStuff(function(err) {
        if (err)
            rrContext.sendError(err, 500);
        else
            rrContext.send('admin', 'saveStuff successful');
    });
}


exports.admin = function(req, res){
    "use strict";
    var command = req.params.command || '';   
    var rrContext = new RRContext(req, res, 'uki/list');
    rrContext.cannedContentType = RRContext.MIME_TYPES.json;
    
    var passwd = req.query['auth'];
    if ('casey' !== passwd)
        rrContext.sendError("Invalid authorization", 403);
    else {
        if ('resetMongodb' === command)
            resetMongodb(rrContext, req.query['more']);
        else if ('purgeOld' === command)
            purgeOld(rrContext);
        else if ('saveStuff' === command)
            saveStuff(rrContext);
        else if ('pretend500' === command)
           rrContext.sendError("pretend500: ", 500);
        else if ('addAuthor' === command)
           addAuthor(rrContext);
        else  // nothing matched.
           rrContext.sendError("Invalid request: " + command, 404);
    }  
};

exports.admin.saveStuff = saveStuff;

exports.submitFW = function(req, res){
   "use strict";
   var rr = new RRContext(req, res);
   var body = req.body;
   var author = global.nextq.mdb.authors[body.author];
   if (!author) {
      rr.sendError("Invalid author code", 403);
      return;
   }
   if (body.deleteID) {
      var query = { _id : body.deleteID, author: author };
      global.nextq.mdb.removeCollections(['fun', 'work'], query, function(err) {
         if (err)
            rr.sendError(err.message, 400);
         else {
            console.log('admin.submitFW:  show deleted  %j', query);
            rr.send('delete', 'successful for show ID= ' + body.deleteID, 'json');
         }
      });
      
      return;
   }
   
   var show = {
      club: body.club,
      org: body.org,
      dates: [new Date(body.date)],
      urls: [body.url],
      location: body.location,
      state: utils.getState(body.location),
      author: author
   };
   show = Show.cleanup(show);
   Show.addLatLon(global.nextq.geocoder, [show], function(err) {
      if (err)
        rr.sendError(err);
      else {
         global.nextq.mdb.insert(show.org, show, function(err) {
            if (err)
               rr.sendError(err.message, 500);
            else {
               console.log('admin.submitFW:  show added %j', show);
               rr.send('submit', 'successful for show ID= ' + show._id, 'json');
            }
         });
      }
   });
};


