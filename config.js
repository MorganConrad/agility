var config = {
    
    //app: null,
    
    mode : 'development',
    version : '0.2.0',
    // loggerFormat: 'short',
    loggerFormat: ':date :ip - :method :url :status - :response-time ms',
    defaultMonths: 3,
    
    db: {
        url: 'todo',
        connectOptions : {},
        autosaveMinutes : 20,
        refreshMinutes: 48 * 60   // 48 hours
    },
    
    log : {
       error : function(err) {
           "use strict";
           console.error(arguments);
       },
       info : function(info) {
            "use strict";
            console.info(arguments);
       }
    },
    
    //myMongo: null
};



module.exports = config;
