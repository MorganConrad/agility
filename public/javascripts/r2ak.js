
var marker1;
var map;
var legend;
function initMap() {
   map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 49.69, lng: -125},  // Courtenay
      zoom: 8
   });

   legend = document.getElementById('legend');

   map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(
      document.getElementById('legend'));

   // map.data.loadGeoJson('http://flyingspaniel.com/buoy/buoyg.py/PTWW1/FRDW1/KECA2');
   //var ALL = "PTWW1/FRDW1/KECA2/SISW1/46088/CHYW1/46134/46146/46131/46203/46185/46183/46145/MRYA2";

   var xmlHttp = new XMLHttpRequest();
   xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
         addBuoyJSON(map, xmlHttp.responseText);
   }
   xmlHttp.open("GET", "http://www.nextq.info/R2AK/buoys", true); // true for asynchronous
   xmlHttp.send(null);


   var xmlHttpLand = new XMLHttpRequest();
   xmlHttpLand.onreadystatechange = function() {
      if (xmlHttpLand.readyState == 4 && xmlHttpLand.status == 200)
         addLandJSON(map, xmlHttpLand.responseText);
   }
   xmlHttpLand.open("GET", "http://www.nextq.info/R2AK/land", true); // true for asynchronous
   xmlHttpLand.send(null);


}


function createMarker(map, station) {
   station = station[0];
   if (!station)
      return;
   var info = station.compat || station;
   var marker = new google.maps.Marker({
      position: { "lat": Number(info.lat), "lng": Number(info.lon) },
      map: map,
      icon: windBarbSVGIcon(station),
      title: "" + station.id,
      scale: 1.0,
      station: station
   });

   marker.addListener('click', function(e) {
      var infowindow = new google.maps.InfoWindow( { content : stationToHTML(this.station) });
      infowindow.open(map, this);
   });
}


function addBuoyJSON(map, json) {
   var buoys = JSON.parse(json);
   for (var id in buoys) {
      var marker = createMarker(map, buoys[id]);
   }
}



function addLandJSON(map, json) {
   var lands = JSON.parse(json);
   for (var id in lands) {
      var marker = createMarker(map, lands[id]);
   }
}




function stationToHTML(station) {
   var info = station.compat || station;

   var html = '<style> table, th, td {border: 1px solid black;border-collapse: collapse;}</style><h3>' + info.name + "</h3>" +
      "<h4>ID#" + info.id + "&nbsp&nbsp&nbsp @ " + info.lat + " N " + (-info.lon) + " W" + "</h4>" +
       table(info);

   html = html + measToRow(info, 'windgust', 'Gusts');
   html = html + measToRow(info, 'waveht', 'Wave Ht');
   html = html + measToRow(info, 'domperiod', 'Dom Period');

   html = html + "</TABLE>";
   return html;
}


function table(station) {
   var table = '<TABLE><tr><th>measurement</th><th>value</th><th>units</th><th>hourly change</th></tr>';
   var meass = ['airtemp','pressure','windspeed','winddir'];
   for (var i=0; i<meass.length; i++) {
      var meas = meass[i];
      table = table + measToRow(station, meas);
   }

   return table;
};


function measToRow(station, meas, userfriendlyname) {
   if (station[meas]) {
      userfriendlyname = userfriendlyname || meas;
      var row = "<tr><td>" + userfriendlyname + "</td><td>" + station[meas] + "</td><td>" + station.uom[meas];
      if (station.hourlyChange && station.hourlyChange[meas])
         row = row + "</td><td>" + station.hourlyChange[meas];

      return row + "</td></tr>";
   }

   return "";
}


// draws as if coming from the north
function windBarbSVGIcon(station) {
   var info = station.compat || station;

   var icon = {
      strokeColor: '#000000',
      strokeOpacity: 1,
      strokeWeight: 3,
      fillColor: '#808080',
      fillOpacity: 1,
      rotation: 0,
      scale: 1.0};

   var path = "M0 0";
   if ("land" === station.type) // it's a land station
      path = path + " L-10 10 L0 20 L10 10 L0 0 "
   else
      path = path + " m -10 0 a 10,10 0 1,0 20,0 a 10,10 0 1,0 -20,0";
   path = path + " L0 -40 "; // draw the wind tail


   // add barbs
   var tail = -40;
   var knots = numericPart(info.windspeed) + 2.5;
   while (knots >= 50.0) {
      var tail10 = tail-10;
      path = path + "M0 " + tail + " L0 " + tail10 + " L30 " + tail10 + " L0 " + (tail);
      tail += 10;
      knots -= 50;
   }

   while (knots >= 10.0) {
      var tail10 = tail-10;
      path = path + "M0 " + tail + " L30 " + tail10;
      tail += 10;
      knots -= 10;
   }

   if (knots >= 5.0) {  // can't loop
      path = path + "M0 " + tail + " L15 " + (tail-5);
   }

   icon.path = path;
   icon.rotation = numericPart(info.winddir);

   var waveHt = numericPart(info.waveht, -1);
   var domPeriod = numericPart(info.domperiod, -1);
   //if (pressureIn > 30)
   //    icon.fillColor = '#00FF00';
   //if (pressureIn < 29.8)
   //    icon.fillColor = '#FF0000';
   //if (pressureIn < 1)
   //    icon.fillColor = '#888888';
   icon.fillColor =  computeWaveColor(waveHt, domPeriod);
   return icon;
}



function numericPart(input, defaultValue) {
   if (!input)
      return defaultValue || 0;
   if (isNaN(input)) {
      var space = input.indexOf(" ");
      if (space > 0) input = input.substr(0, space);
   }
   return Number(input);
}



function computeWaveColor(ht, period) {
   if ((ht < 0) && (period <= 0))
      return "#808080";

   if (ht < 4)
      return '#00FF00';
   if (ht > 8)
      return '#FF0000';
   return '#FFFF00'
   // TODO - figure out period as well
}


//marker1 = new google.maps.Marker({
//   position: { lat: 48.111, lng: -122.76 },
//   map: map,
//   title: "PTWW1"
//});

//marker1.addListener('click', function() {
//   var contentString = "this is the info box crap";
//   var infowindow = new google.maps.InfoWindow( { content : contentString });
//   infowindow.open(map, marker1);
//
//});

