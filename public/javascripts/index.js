// javascripts for index.html

function onSubmitSearch() {
   var vmap = $('#us-map').vectorMap('get', 'mapObject');
   var stateArray = vmap.getSelectedRegions();
   if ( (stateArray.length === 0) || (stateArray.length > 10)) {
      alert('Please select one to ten states.');
      return false;
   }

   var stateString = JSON.stringify(stateArray);

   var docstates = document.getElementById("states");
   docstates.value = stateString;

   if (localStorage) {
      var orgcount = 0;
      var cbs = document.getElementsByClassName("LS.cb");
      for (var i=0; i<cbs.length; i++) {
         var cb = cbs[i];
         localStorage.setItem('cb.'+cb.id, cb.checked ? 'true' : '');
         if (cb.name === 'orgs' && cb.checked)
            orgcount++;
      }
      
      if (!orgcount) {
         alert('Please select one or more clubs.');
         return false;
      }

      var nfs = document.getElementsByClassName("LS.nf");
      for (i=0; i<nfs.length; i++) {
         localStorage.setItem('nf.'+nfs[i].id, nfs[i].value || '');
      }

      localStorage.setItem('selected-states', stateString);
   }

   return true;
}

function datePlus(months) {
   var date = new Date();
   date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
   date.setMonth(date.getMonth() + months);
   return date.toJSON().substr(0,10);
}

function onCheckboxClick(name, checked) {
   localStorage.setItem('cb.'+name, checked ? 'true' : '');
}

function onWithinClick(name, checked) {
   document.getElementById("miles").disabled = !checked;
  // document.getElementById("near").disabled = !checked;
   // onCheckboxClick(name, checked);
}

function initialChecked(name) {
   return 'true' === localStorage.getItem('cb.'+name);
}


function onIndexReady(jQuery) {

   var statesStr = null;

   if (localStorage) {
      var cbs = document.getElementsByClassName("LS.cb");
      for (var i=0; i<cbs.length; i++) {
         cbs[i].checked = 'true' === localStorage.getItem('cb.'+cbs[i].id);
      }

      var checked = document.getElementById("within").checked;
      document.getElementById("miles").disabled = !checked;
      // document.getElementById("near").disabled = !checked;

      var nfs = document.getElementsByClassName("LS.nf");
      for (i=0; i<nfs.length; i++) {
         var value = localStorage.getItem('nf.' + nfs[i].id);
         if (value) nfs[i].value = value;
      }

      statesStr = localStorage.getItem('selected-states');
   }
  
   var statesArray = statesStr ? JSON.parse(statesStr) :  [];
   jQuery('#us-map').vectorMap({
      map: 'usca_lcc_en',
      zoomOnScroll: false,
      regionsSelectable: true,
      onRegionSelected: function(e, code, isSelected, selectedArray) {
         localStorage.setItem(
            'selected-states',
            JSON.stringify(selectedArray)
         );
      },
      selectedRegions : statesArray
   });

   document.getElementById('start').value = datePlus(0);
   document.getElementById('start').valueAsDate = new Date();
   document.getElementById('end').value = datePlus(3);
   var plus3 = new Date(); plus3.setMonth(plus3.getMonth() + 3);
   document.getElementById('end').valueAsDate = plus3;
};
