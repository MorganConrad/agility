var interestMap = {};

var getInterest = function(id) {
   return interestMap[id] || 'No';
};

function setInterest(cb) {
   var id = cb.getAttribute('sid');
   if (cb.checked){
      interestMap[id] = '1';
   }
   else
      delete interestMap[id];

   cb.textContent = cb.checked? '1' : '';  // required so sorting works
   // or now, always store immediately
   if (localStorage)
      localStorage.setItem('interestMap', JSON.stringify(interestMap));
}

function onShowlistReady(jq) {
   var deleteCount = 0;
   var date = new Date();
   var datenum = (date.getFullYear()-2000)*10000 + (date.getMonth()+1)*100 + date.getDate();
   var datestr = '' + datenum;
   var str = localStorage ? localStorage.getItem('interestMap') : '{}';
   interestMap = str ? JSON.parse(str) :  {};
   
   for (var key in interestMap) {  // purge old dates
      if (key < datestr) {
         deleteCount++;
         delete interestMap[key];
      }
   }  
   if (deleteCount > 0)
      localStorage.setItem('interestMap', JSON.stringify(interestMap));
   
   jq('.sinterest').each(function(idx, cb) {
      cb.checked = '1' === interestMap[cb.getAttribute('sid')];
      cb.textContent = cb.checked ? '1' : ''; // required so sorting works
   });
}

function onSnapshot() {
   var snap = document.getElementById("snap");
   snap.value = Object.keys(interestMap).join('+');
   if (snap.value.length === 0) {
      alert('Please select one or more shows as your favorites');
      return false;
   }
   var near = document.getElementById("near");
   near.value = localStorage.getItem('nf.near') || '';
   return true;
}

function clearInterests() {
   $('.sinterest').each(function(idx, cb) {
      cb.checked = false;
      cb.textContent = ''; // required so sorting works
   });

   interestMap = {};
   if (localStorage)
      localStorage.setItem('interestMap', '{}');
   
   return false;  // do not submit form
}


function onBF1(event, location) {   
   var directBF = false;
   if (localStorage)
      directBF = localStorage.getItem('directBF');

   trackClick('outbound','bringfido',{'location': event.currentTarget.href});
      
   if (directBF) {
      event.preventDefault();
      var href = event.currentTarget.href + '?directBF=true';
      window.location.href = href;
   }
   
   // else normal processing...
}


function trackClick(category, label, fields) {
   if (fields)
      ga('send', 'event', category, 'click', label, fields);
   else
      ga('send', 'event', category, 'click', label);
   return true;
}

