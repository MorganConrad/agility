// javascripts for m.html  (the mobile form)

function onSubmitSearch() {
   var statechooser = document.getElementById("m_chooser");
   var stateArray = [];
   for (var i= 0, iLen=statechooser.options.length;  i<iLen; i++) {
      var opt = statechooser.options[i];
      if (opt.selected)
         stateArray.push(opt.value);
   }
   if ( (stateArray.length === 0) || (stateArray.length > 5)) {
      alert('Please select one to five states');
      return false;
   }
   var stateString = JSON.stringify(stateArray);
   var docstates = document.getElementById("states");
   docstates.value = stateString;
   statechooser.value = 'y';
   
   var orgcount = 0;
   if (localStorage) {
      var cbs = document.getElementsByClassName("LS.cb");
      for (var i=0; i<cbs.length; i++) {
         var cb = cbs[i];
         localStorage.setItem('cb.'+cb.id, cb.checked ? 'true' : '');
         if (cb.name === 'orgs' && cb.checked)
            orgcount++;
      }

      if (!orgcount) {
         alert('Please select one or more clubs.');
         return false;
      }

      var nfs = document.getElementsByClassName("LS.nf");
      for (i=0; i<nfs.length; i++) {
         localStorage.setItem('nf.'+nfs[i].id, nfs[i].value || '');
      }

      localStorage.setItem('selected-states', stateString);
   }

   return true;
}

function datePlus(months) {
   var date = new Date();
   date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
   date.setMonth(date.getMonth() + months);
   return date.toJSON().substr(0,10);
}

function onCheckboxClick(name, checked) {
   localStorage.setItem('cb.'+name, checked ? 'true' : '');
}

function onWithinClick(name, checked) {
   document.getElementById("miles").disabled = !checked;
  // document.getElementById("near").disabled = !checked;
   // onCheckboxClick(name, checked);
}

function initialChecked(name) {
   return 'true' === localStorage.getItem('cb.'+name);
}


function onIndexReady(jQuery) {
   var statesStr = '';

   if (localStorage) {
      statesStr = localStorage.getItem('selected-states');
      var cbs = document.getElementsByClassName("LS.cb");
      for (var i=0; i<cbs.length; i++) {
         cbs[i].checked = 'true' === localStorage.getItem('cb.'+cbs[i].id);
      }

      var checked = document.getElementById("within").checked;
      document.getElementById("miles").disabled = !checked;
      // document.getElementById("near").disabled = !checked;

      var nfs = document.getElementsByClassName("LS.nf");
      for (i=0; i<nfs.length; i++) {
         var value = localStorage.getItem('nf.' + nfs[i].id);
         if (value) nfs[i].value = value;
      }
   }

   document.getElementById('start').value = datePlus(0);
   document.getElementById('start').valueAsDate = new Date();
   document.getElementById('end').value = datePlus(3);
   var plus3 = new Date(); plus3.setMonth(plus3.getMonth() + 3);
   document.getElementById('end').valueAsDate = plus3;
   
   var statechooser = document.getElementById('m_chooser');
   for (var i= 0, iLen=statechooser.options.length;  i<iLen; i++) {
      var opt = statechooser.options[i];
      opt.selected = opt.value.length && statesStr.indexOf(opt.value) >= 0;
   }

};
