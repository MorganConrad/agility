
/**
 * Module dependencies.
 */

var express = require('express');

var http = require('http');
var path = require('path');

// new Express 4 requires
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var compression = require('compression');
var errorHandler = require('errorhandler');

var admin = require('./routes/admin');
var MDB = require('./lib/mdb');
var config = require('./config');
var yw = require('./routes/yw');
var bringfido = require('./routes/bringfido');
var search = require('./routes/search');
// var Yahoo = require('./lib/yahoo');
var GoogleGC = require('./lib/googlegc');
var RRContext = require('./lib/rrcontext');
var utils = require('./lib/utils');
var Show = require('./lib/show');

const Kepi = require('kepi');


// var R2AK = require('./routes/R2AK');
var app = express();

setupConfig(config);

global.nextq = {
   app: app,
   config: config
};

// configure our geocoder
//global.nextq.geocoder =  new Yahoo(config.GEO_APPID);
global.nextq.geocoder =  new GoogleGC(config);

Show.GEOCODER = global.nextq.geocoder;
   
// all environments
app.set('port', config.port || 3001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(favicon(path.join(__dirname, 'public/images/favicon.ico')));

if (config.isOpenshift) {
   app.use(ignoreHeartbeat(1800));
}
app.use(logger(config.loggerFormat || 'dev'));

const kepi = Kepi({"Referrer-Policy": 'no-referrer'}).safe();
kepi.contentSecurityPolicy()
  .add('script-src', "'self'", "'unsafe-inline'", "http://www.google-analytics.com", "http://s7.addthis.com")
  .add('style-src', "'self'", "'unsafe-inline'")
  .add('img-src', "*");
kepi.featurePolicy().add('microphone', "'none'");  // make SecurityHeader.com happier
app.use(kepi.middleware());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(compression({ threshold: 20480 }));  // mainly compress video stuff...
//app.use(app.router); // ??? TODO???
app.use(express.static(path.join(__dirname, 'public')));

logger.token('ip', function(req, res){
   "use strict";
   return req.headers["x-forwarded-for"] ? req.headers["x-forwarded-for"].split(',').pop() : req.connection.remoteAddress;
});

// app.get('/R2AK/land', R2AK.land);
// app.get('/R2AK/buoys', R2AK.buoys);

app.get('/ag/search', search.search);
app.post('/ag/search', search.search);
app.get('/v1/ag/search', search.search);

// currently unused
app.get('/ag/expand/:id', search.search1);

app.get('/ag/search/:ids', search.searchByIDsInParams);
app.get('/ag/snap', search.searchByIDsInQuery);
app.post('/ag/snap', search.searchByIDsInQuery);

app.post('/ag/submitFW', admin.submitFW);

app.get('/admin/:command', admin.admin);
app.get('/v1/admin/:command', admin.admin);

// app.get('/rd/yw/:location', yw.ywRedirect(global.nextq.geocoder));  // currently unused
app.get('/rd/bf/:location', bringfido.bfRedirect);

// first attempt at mobile support...
app.get('/m', function(req, res, next) {
   "use strict";
   res.redirect('/m.html');
});

// development only
if ('development' === app.get('env')) {
   app.use(errorHandler());
}

// non development handle 404s
app.use(function(req, res, next) {
   "use strict";
   res.status(404);
   var error = { url : req.url };
   
   if (req.accepts('html')) {
      res.render('404.jade', { url: req.url });
      return;
   }
   
   if (req.accepts('json')) {
      res.send({ error: 'Not found: ' + req.url });
      return;
   }
   
   res.type('txt').send('Not found: ' + req.url);
});


// configure mongoDB and get started

var myMongo = new MDB(config.mongoURI, 'nextq', config.db, global.nextq.geocoder, function(err) {
   "use strict";
   if (!err) {
      myMongo.startUp(function(err) {
         if (err) {
            console.error(err);
            console.error(utils.nicerDateString() + ' ERROR Cannot startUp mongodb at ' + JSON.stringify(config.db, null, 2));
         }
         else {
            app.listen(config.port, config.ipAddress, function(){
               utils.logWithDate('  NextQ listening on port ' + config.port);
            });

            if (config.isModulus)
               app.listen(63002, shutdownHook);
         }
      });
   }
   else {
      console.error(err);
      console.error(utils.nicerDateString() + ' ERROR Cannot connect to mongodb at ' + JSON.stringify(config.db, null, 2));
   }
});

global.nextq.mdb = myMongo;


function setupConfig(config) {
   "use strict";
   if (process.env.HEROKU) {
      config.port = process.env.PORT || 5000;
      config.ipAddress = null;
      config.mongoURI = process.env.MONGO_URL;
      config.isHeroku = process.env.HEROKU;
   }
   else if (process.env.OPENSHIFT3) {
      config.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
      config.ipAddress = '0.0.0.0';
      config.mongoURI = process.env.MONGO_URL;
      config.isOpenshift = process.env.OPENSHIFT3;
   }
   else if (process.env.OPENSHIFT_APP_DNS) { // v2
      config.port = process.env.OPENSHIFT_NODEJS_PORT;
      config.ipAddress = process.env.OPENSHIFT_NODEJS_IP;
      config.mongoURI = process.env.OPENSHIFT_MONGODB_DB_URL;
      config.isOpenshift = process.env.OPENSHIFT_APP_DNS;
   }
   else if (process.env.MODULUS_IAAS) {  // modulus
      config.port = process.env.PORT;
      config.ipAddress = '0.0.0.0';
      config.mongoURI = process.env.MONGO_URI;
      config.isModulus = process.env.MODULUS_IAAS;
   }
   else {  // local
      config.port = 3001;
      config.ipAddress = '127.0.0.1';
      config.mongoURI = process.env.MONGOLAB_URI;

      // delete before checkin!!!
      // config.mongoURI = 'mongodb+srv://flying:oCasey8@nextq.7znfm.mongodb.net/test?authSource=admin&replicaSet=atlas-rup2ej-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true';
      config.isLocal = true;
   }

   // always
   config.GEO_APPID = process.env.GOOGLE_APPID;

   // console.dir(config);
   
   return config;
}



// 
function shutdownHook(req, res) {
   "use strict";
   // in theory, we could look at req.body...
   var rrContext = new RRContext(req, null);  // null "response"
   admin.admin.saveStuff(rrContext);
   utils.logWithDate('  shutdownHook called');
}


function ignoreHeartbeat(except) {
   "use strict";
   except = except || 0;
   var count = 1;
   return function (req, res, next) {
      if (req.headers["x-forwarded-for"])
         return next();

      if (except > 0) {
         if (--count <= 0) {
            count = except;
            return next();
         }
      }
      res.end();
   };

}

function createMongoURLforOpenShift(mockEnv) {
   var myEnv = mockEnv || process.env;
   var url = myEnv.OPENSHIFT_MONGODB_DB_URL || myEnv.MONGO_URL;  // check for v2
   if (url)
      return mongoURL;

   var serviceName_ = myEnv.DATABASE_SERVICE_NAME.toUpperCase()+"_";
   
   function env(key) {
      return processEnv[serviceName_ + key] || "<" + key + "?>";
   }

   url = "mongodb://" + env('USER') + ":" + env('PASSWORD') + "@"
                      + env('SERVICE_HOST') + ":" + env('SERVICE_PORT') + "/" + env('DATABASE');

   return url;
}

