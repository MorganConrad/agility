
function WORK() {}

WORK.bringUpToDate = function(rrContext, states, callback) {
   "use strict";

   callback(null, rrContext, states);
};

module.exports = WORK;