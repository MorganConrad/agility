const Request = require('request');
const MyRequest = require('../lib/myrequest');
const Cheerio = require('cheerio');

const Utils = require('../lib/utils');
const Show = require('../lib/show');


const NADAC_ROOT = 'https://www.nadac.com/';
// var NADAC_PATH = 'trial-calendar.htm';
const NADAC_PATH = 'index.php/eventlist/';

function NADAC() {}

NADAC.orgKey = 'nadac';


function parseShow(cheerio, index, ptag) {
  try {
    let show = new Show(NADAC.orgKey);
    let spans = cheerio(ptag).find('span');
    show.dates = parseDates(spans[0].children[0].data);
    show.location = spans[1].children[0].data;
    show.state = Utils.getState(show.location);
    let clubpart = spans[2].next;

    show.club= clubpart.children ? clubpart.children[0].data : clubpart.data;
    show.urls = [spans[7].next.attribs.href];
    return Show.cleanup(show);
  }
  catch (err) {
    console.warn("Error in NADAC.parseShow(%d) : %j", index, err);
    return null;
  }
}




/**
 * Parse an individual show contained in a <p> tag
 * @param ptag   incoming paragraph <p> tag
 * @returns {{}}  object representing the show
 */
function oldparseShow(cheerio, index, ptag) {
   "use strict";
   try {
      var show = new Show(NADAC.orgKey);

      // eventually make cheerio(ptag).find('strong')
      var children = ptag.children;

      // child[0] is date, child[2] is location    

      show.dates = parseDates(Utils.childDataTrimmed(ptag, 0, 0));

      // bug 9 look for extremely old dates
      show.dates[0]



      show.location = Utils.childDataTrimmed(ptag, 2, 0);
      show.state = Utils.getState(show.location);

      // something changed recently...
      show.club = Utils.childDataTrimmed(ptag, 3);

      for (var c = 3; c < children.length; c++) {
         var child = children[c];
         /*
          Within each paragraph <p>, there are <br>s.
          Within each <br>,
          there is a <strong> with the keyword, e.g. "Ring Size:",
          followed by plaintext with the value, e.g. '90x100'
          */
         if ('strong' === child.name) {
            // last child of <strong> has the text for the key
            var schildren = child.children;
            var lastschild = schildren[schildren.length - 1];
            var key = lastschild.data.trim();
            if (':' === key.charAt(key.length - 1))
               key = key.substr(0, key.length - 1);

            var valuenode = child.next;
            // special case - check if the value is actually an <a href> tag, if so, get text from it's child
            var value = ('a' === valuenode.name) ? valuenode.children[0].data : valuenode.data;

            show[key] = value.trim();
         }
      }

      // convert Website to url
      var mayBeNullURL = Utils.fixLink(show.Website);
      show.urls = Utils.toArray(mayBeNullURL);  // might be null  (bug #4)
      // just in case there is no judge
      show['Judge(s)'] = show['Judge(s)'] || 'TBA';
      delete show.Website;

      return Show.cleanup(show);
   }
   catch (err) {
      console.warn("Error in NADAC.parseShow(%d) : %j", index, err);
      return null;
   }
}


NADAC.parseHTML = function(html) {
    "use strict";
    var shows = [];
    var cheerio = Cheerio.load(html);
    var showsDiv = cheerio('.et_pb_code_inner');

    cheerio('p', showsDiv).map(function(i, link) {
       var show = parseShow(cheerio, i, link);
       if (show)
          shows.push(show);
    });

    return shows;
};

NADAC.oldparseHTML = function(html) {
  "use strict";
  var shows = [];
  var cheerio = Cheerio.load(html);
  var span12 = cheerio('.content .span12');

  cheerio('p', span12).map(function(i, link) {
    var show = parseShow(cheerio, i, link);
    if (show)
      shows.push(show);
  });

  return shows;
};


// obsolete
function parseRequest1(rrContext) {
    "use strict";

    return function (error, response, body) {

        rrContext.onResponse(error, response);

        if (rrContext.hadError()) {
            rrContext.send(null, null);
            return;
        }

        var shows = NADAC.parseHTML(body);

        rrContext.send('shows', shows);

       global.nextq.mdb.mergeAndSave(shows, NADAC.orgKey);
    };
}



// NADAC uses messy format, Dec. 27-29 2013
function parseDates(rawdate) {
    "use strict";
    var mdy = rawdate.split(' ');
    var month = mdy[0].substr(0,3) + ' ';
    var year = ' ' + mdy[2].trim();
    var ddd = mdy[1].split('-');
    var dd2 = ddd.length > 1 ? ddd[1].trim() : '';

    var date0 = new Date(month + ddd[0] + year);
    var date1 = dd2.length > 0 ? new Date(month + dd2 + year) : null;

    return Utils.contigDates(date0, date1);
}



NADAC.bringUpToDate = function(rrContext, states, callback) {
    "use strict";
  let outOfDates = states;
  if (Utils.allowCachedShows) {
    outOfDates = global.nextq.mdb.getOutOfDates(NADAC.orgKey, states);
    if (outOfDates.length === 0)
      return callback(null, rrContext, states);
  }
    
    else {
        var requestInfo = {
            uri: NADAC_ROOT + NADAC_PATH,
            method: "POST",
            form : {
                Submit: 'Search',
                showrange: rrContext.req.params.showrange || '999',
                submitted: 'yes'
            }
        };

        requestInfo.form['stateprov[]'] = outOfDates;

        var usedForSideEffect = new MyRequest(requestInfo, function (error, response, body) {
            rrContext.onResponse(error, response);

            if (rrContext.hadError()) {
                // TODO figure this out...
                callback(rrContext.getError(), rrContext);
                return;
            }

            var shows = NADAC.parseHTML(body);

           global.nextq.mdb.addNewShows(shows, NADAC.orgKey, outOfDates, callback, function(err, mergedShows) {
                callback(err, rrContext, states);
            });

           Utils.logWithDate('NADAC brought up to date for states: ' + JSON.stringify(states));
        });
    }

};

module.exports = NADAC;
