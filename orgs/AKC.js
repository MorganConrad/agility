

const Request = require('request'),
   Utils = require('../lib/utils'),
   CHUtils = require('../lib/ch-utils'),
   Show = require('../lib/show'),
   Cheerio = require('cheerio');

const AKC_APPS_ROOT = 'http://www.apps.akc.org';

// needs &states=xx,yy at the end  TODO CURRYR might fail in December???
const AKC_SEARCH = 'http://www.apps.akc.org/events/search/blocks/dsp_event_list.cfm?active_tab_row=2&active_tab_col=4&fixed_id=12&club_name=&date_range=FUTURE&event_grouping=AG&save_as_default=Y&saved_states=&select_all=&states=';

var nowMS;

function AKC() {}

AKC.orgKey = 'akc';

function parseShow(index, tableRow) {
    "use strict";
   try {
      var show = new Show(AKC.orgKey);
      var tds = Cheerio(tableRow).children('td');
      var dateStr = CHUtils.text(tds[3]);

      show.dates = [new Date(dateStr)];
      if (show.dates[0].getTime() < nowMS)  // AKC web site has past shows, don't process them
         return null;
      
      // 1st td has name, event number and status
      var td0 = tds.eq(0);
      var hugelink = td0.find('.white').eq(0); // [0];
      
      var href = hugelink.attr('href');
      var start = href.indexOf('openWin(') + 9;
      var end = href.indexOf("',", start);
      show.urls = [AKC_APPS_ROOT + href.substr(start, end - start)];  
      show.club = hugelink.text();

      show.eventNumbers = [Utils.childDataTrimmed(td0[0], 3, 1)];
      show.status = Utils.childDataTrimmed(td0[0], 5, 1);

      show.location = CHUtils.text(tds[1]);
      show.state = Utils.getState(show.location);

      return show;
   }
   catch (err) {
      console.warn("Error in AKC.parseShow(%d) : %j", index, err);
      return null;
   }
   //return Show.cleanup(show); done after merge
}


AKC.parseHTML = function (html) {
  "use strict";
  let shows = [];
  nowMS = Date.now();
  let root = Cheerio.load(html);

  let table2 = Cheerio.load(html)('.qs_table[bgcolor="#71828A"]');
  let trows = table2.find('tr[bgcolor="#FFFFFF"]');
  trows.each(function (i, tablerow) {
    let show = parseShow(i, tablerow);
    if (show)
      shows.push(show);
  });


  return AKC.mergeShows(shows);
};


AKC.mergeShows = function (shows) {
  "use strict";
  if (!shows.length)
    return shows;

  let hash = {};  // a multimap
  shows.forEach(function (show) {
    let key = show.club + '___' + show.location;
    let arr = hash[key];
    if (arr) {
      let lastshow = arr[arr.length - 1];
      if (Utils.datesWithin(lastshow.dates[lastshow.dates.length - 1], show.dates[0], 1)) {
        // shows can be merged
        lastshow.dates.push(show.dates[0]);
        lastshow.urls.push(show.urls[0]);
        lastshow.eventNumbers.push(show.eventNumbers[0]);
      }
      else
        arr.push(show);
    }
    else
      hash[key] = [show];
  });

  let merged = [];
  for (let key in hash) {
    let arr = hash[key];
    for (let i = 0; i < arr.length; i++)
      merged.push(Show.cleanup(arr[i]));  // cleanup here
  }

  return merged;
};



AKC.bringUpToDate = function(rrContext, states, callback) {
  "use strict";
  let outOfDates = states;
  if (Utils.allowCachedShows) {
    outOfDates = global.nextq.mdb.getOutOfDates(AKC.orgKey, states);
    if (outOfDates.length === 0)
      return callback(null, rrContext, states);
  }

  let stateQuery = states.join();
  Request.get(AKC_SEARCH + stateQuery, function (error, response, body) {

    rrContext.onResponse(error, response);

    if (rrContext.hadError()) {
      // TODO figure this out...
      callback(rrContext.getError(), rrContext);
      return;
    }

    let shows = AKC.parseHTML(body);

    global.nextq.mdb.addNewShows(shows, AKC.orgKey, states, function (err, mergedShows) {
      callback(err, rrContext, states);
    });
    Utils.logWithDate('AKC brought up to date for states: ' + JSON.stringify(states));
  });

};


module.exports = AKC;
