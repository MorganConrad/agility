const Request = require('request');
const Cheerio = require('cheerio');

const CHUtils = require('../lib/ch-utils');
const Utils = require('../lib/utils');
const Show = require('../lib/show');


const CPE_ROOT = 'http://www.k9cpe.com/';
// var CPE_PATH = 'events.htm';
const CPE_PATH = 'agilityevents.htm';

function CPE() {}

CPE.orgKey = 'cpe';

var year = ' ' + new Date().getUTCFullYear();  // also part of Bug #7


/**
 * Parse an individual show contained in a <tr> tag
 * @param tr   table row
 * @returns {{}}  object representing the show
 */
function parseShow(index, tr) {
   "use strict";
   try {
      var show = new Show(CPE.orgKey);
      var cheerioRow = Cheerio(tr);
      var tds = cheerioRow.children('td');

      if (!tds.length) {  // this is a year row
         year = ' ' + cheerioRow.text().trim();
         return null;
      }
      
      show.dates = parseDates(CHUtils.text(tds[0]), year);
      show.state = CHUtils.text(tds[1]);
      var town = CHUtils.text(tds[2]);
  // note, has two preceeding &nbsp; as well  was removed earlier in .parseHTML()
  //    if (indoors > 12)
 //        town = town.substr(0, indoors - 12);
      show.location = town + ', ' + show.state;
      show.club = CHUtils.text(tds[3])

      // column 4 contains <td><a href="#Paws&QuesFL1242014">Details</a>&nbsp;<a href="http://www.k9cpe.com/premiums/012414flpawsnques.pdf">

      show.urls = [CPE_ROOT + CPE_PATH + tds[4].children[0].attribs.href];

      // not all shows have a PDF
      var pdf = tds[4].children[2] ? tds[4].children[2].attribs.href : null;
      if (pdf)
         show.urls.push(pdf);

      return Show.cleanup(show);
   }
   catch (err) {
      console.warn("Error in CPE.parseShow(%d) : %j", index, err);
      return null;
   }
}



CPE.parseHTML = function(html) {
    "use strict";
    var shows = [];
    
    // all the <B>s are a pain in the butt, eliminate them
    var noBs = html.replace(/<\/?b>/gi, '');
    noBs = noBs.replace(/&nbsp;&nbsp;\(Indoors\)/gi, '');  // also remove all the (Indoors)
    
    var table2 = Cheerio.load(noBs)('table[border="1"]');
    var trows = table2.find('tr');
    trows.each(function (i, tr) {
        var show = parseShow(i, tr);
        if (show)
           shows.push(show);
    });

    return shows;
};




// CPE uses format, Dec. 27-29, with  multimonth Aug 30-31 & Sep 1
function parseDates(rawdate, year) {
    "use strict";
    var multimonth = rawdate.split('&');  // across months???
    if (multimonth.length > 1) {
        var datesMonth0 = parseDate(multimonth[0].trim(), year);
        var datesMonth1 = parseDate(multimonth[1].trim(), year);
        return Utils.contigDates(datesMonth0[0], datesMonth1[1]);
    }
    
    else {
      var dates = parseDate(multimonth[0], year);
      return Utils.contigDates(dates[0], dates[1]);
    }
}


function parseDate(dateStr, year) {
    "use strict";
    year = year || ' 2014';
    var month = dateStr.substr(0,4);

    dateStr = dateStr.substring(4);
    var ddd = dateStr.split('-');
    var dd2 = ddd.length > 1 ? ddd[1].trim() : '';

    var date0 = new Date(month + ddd[0] + year);
    var date1 = dd2.length > 0 ? new Date(month + dd2 + year) : date0;

    return [date0, date1];
}



CPE.bringUpToDate = function(rrContext, states, callback) {
    "use strict";

    // for CPE we always use All states and countries for uptodateness   
    var outOfDates = global.nextq.mdb.getOutOfDates(CPE.orgKey, ['All']);
    if (Utils.allowCachedShows && outOfDates.length === 0)
        callback(null, rrContext, states);

    else {

        Request.get( CPE_ROOT + CPE_PATH, function (error, response, body)  {

            rrContext.onResponse(error, response);
            
            if (rrContext.hadError()) {
                // TODO figure this out...
                callback(rrContext.getError(), rrContext, global.nextq.mdb.showCache[CPE.orgKey]);
                return;
            }

            var shows = CPE.parseHTML(body);

           global.nextq.mdb.addNewShows(shows, CPE.orgKey, ['All'], callback, function(err, mergedShows) {
                callback(err, rrContext, states);
            });

            Utils.logWithDate('CPE brought up to date for states: ' + JSON.stringify(states));
        });
    }

};

module.exports = CPE;

