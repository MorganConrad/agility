const Request = require('request'),
   Cheerio = require('cheerio'),
   Utils = require('../lib/utils'),
   CHUtils = require('../lib/ch-utils'),
   Show = require('../lib/show');

const USDAA_ROOT = 'https://www.usdaa.com/';
// const USDAA_PATH = 'events.cfm';
const USDAA_PATH = 'events/event-calendar.cfm';

function USDAA() {}

USDAA.orgKey = 'usdaa';

/**
 * Parse an individual show contained in a tr
 * @param cheerio
 * @param tr   incoming tablerow <tr> tag
 * @returns {{}}  object representing the show
 */
function parseShow(index, tr) {
   "use strict";
   try {
      var show = new Show(USDAA.orgKey);

      var tds = Cheerio(tr).children('td');

      // 0th has dates
      // 1st has club, also a href  fr the url (a show number)
      // 2nd has location
      // 3rd has "type", e.g. League PLay

      show.dates = parseDates(CHUtils.text(tds[0]));
      var clubAndURL = tds.eq(1);
      show.club = clubAndURL.text().trim();
      show.urls = CHUtils.findAHRefs(clubAndURL, USDAA_ROOT);
      show.location = CHUtils.text(tds[2]);
      //show.state = Utils.getState(show.location);
      // USDAA now puts state first
      show.state = (show.location+"  ").substr(0,2);

      return Show.cleanup(show);
   }
   catch (err) {
      console.warn("Error in USDAA.parseShow(%d) : %j", index, err);
      return null;
   }
}


USDAA.oldparseHTML = function(html) {
    "use strict";
    var shows = [];
    var corrected = html.replace(/<th><strong>/gi, '<td><strong>');  // bug in their HTML

    var table2 = Cheerio.load(corrected)('table[id="eventCalendar"]');
    var trows = table2.find('tr');
    
    trows.each(function (i, tr) {
       if (i > 0) {  // skip first row
          var show = parseShow(i, tr);
          if (show)
             shows.push(show);
       }
    });

    return shows;
};

USDAA.parseHTML = function(html) {
  "use strict";
  let shows = [];

  let table2 = Cheerio.load(html)('table[id="eventCalendar"]');
  let trows = table2.find('tr');

  trows.each(function (i, tr) {
    let show = parseShow(i, tr);
    if (show)
      shows.push(show);
  });

  return shows;
};


// USDAA uses MM/DD/YYYY -  MM/DD/YYYY format
function parseDates(rawdate) {
    "use strict";
    var split = rawdate.split('-');
    var date0 = parse1Date(split[0]);
    var date1 = split.length > 1 ? parse1Date(split[1]) : null;

    return Utils.contigDates(date0, date1);
}

function parse1Date(str) {
    "use strict";
    var split = str.trim().split('/');
    if (split.length >= 2)
        return new Date(split[2], split[0]-1, split[1]);
    return null;
}



USDAA.bringUpToDate = function(rrContext, states, callback) {
    "use strict";
   
   // USDAA always use 'All' for uptodateness... 
    var outOfDates = global.nextq.mdb.getOutOfDates(USDAA.orgKey, ['All']);
    if (Utils.allowCachedShows && outOfDates.length === 0)
        callback(null, rrContext, states);
    
    else {
        Request.get( USDAA_ROOT + USDAA_PATH, function (error, response, body) {

            rrContext.onResponse(error, response);

            if (rrContext.hadError()) {
                // TODO figure this out...
                callback(rrContext.getError(), rrContext, global.nextq.mdb.showCache[USDAA.orgKey]);
                return;
            }

            var shows = USDAA.parseHTML(body);
           global.nextq.mdb.addNewShows(shows, USDAA.orgKey, ['All'], function(err, mergedShows) {
                callback(err, rrContext, states);
            });

           Utils.logWithDate('USDAA brought up to date for states: ' + JSON.stringify(states));
        });
    }
};



module.exports = USDAA;