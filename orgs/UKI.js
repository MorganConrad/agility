const Request = require('request'),
   Cheerio = require('cheerio'),
   Utils = require('../lib/utils'),
   CHUtils = require('../lib/ch-utils'),
   Show = require('../lib/show');

const UKI_ROOT = 'https://entries.ukagilityinternational.com/';
const UKI_PATH = 'ShowDiary.aspx';

function UKI() {}

UKI.orgKey = 'uki';

/**
 * Parse an individual show contained in a <tr> tag
 * @param index ignored
 * @param tr   incoming tablerow <tr> tag
 * @returns {{}}  object representing the show
 */
function parseShow(index, tr) {
   "use strict";
   try {
      var show = new Show(UKI.orgKey);

      var tds =  Cheerio(tr).children('td');

      show.dates = parseDates(CHUtils.text(tds[0]));
      show.club = CHUtils.text(tds[1]);
      show.location = CHUtils.text(tds[2]);
      show.state = Utils.getState(show.location);
      if (show.state === 'Place')
        show.state = '**';
        // 3, 4 are open close dates, ignore for now

      // 5 has url in a ShowID=XXX format
      show.urls = CHUtils.findAHRefs(tds.eq(5), UKI_ROOT);

      return Show.cleanup(show);
   }
   catch (err) {
      console.warn("Error in UKI.parseShow(%d) : %j", index, err);
      return null;
   }
}


UKI.parseHTML = function(html) {
    "use strict";
    var shows = [];
    var table2 = Cheerio.load(html)('table[id="ctl00_cpMain_tblShows"]');
    var trows = table2.find('tr');
    /**
     * each *odd* table row is a show
     */
    trows.each(function (i, tr) {
       if (i) {  // skip first row
            var show = parseShow(i, tr);
            if (show)
                shows.push(show);
        }
    });

    return shows;
};


function parseDates(rawdate) {
    "use strict";
    var split = rawdate.split('-');
    if (split.length === 1)
       return [new Date(rawdate.trim())];

    var date0, date1;
    
    // multiple dates, see if it is two complete dates?
    if (split[0].indexOf('201') >= 0) {
        date0 = new Date(split[0]);
        date1 = new Date(split[1]);
        return Utils.contigDates(date0, date1);
    }
    
    // its something like Jan 25-26, 2014
    var month = split[0].substr(0,4);  // with trailing space
    var year = split[1].substr(split[1].length-5);  // with leading space
    
    date0 = new Date(split[0] + year);
    date1 = new Date(month + split[1]);

    return Utils.contigDates(date0, date1);
}



UKI.bringUpToDate = function(rrContext, states, callback) {
    "use strict";
    
    // for UKI we always use All states and countries for uptodateness   
    var outOfDates = global.nextq.mdb.getOutOfDates(UKI.orgKey, ['All']); 
    if (Utils.allowCachedShows && outOfDates.length === 0)
       callback(null, rrContext, states);
    
    else {
        var requestInfo = {
            uri: UKI_ROOT + UKI_PATH,
            method: "POST",
            form : {}
        };

        requestInfo.form['ctl00$cpMain$StateProvince'] = '';
        requestInfo.form['ctl00$cpMain$Month'] = rrContext.req.params.ctl00$cpMain$Month || '';
        requestInfo.form['ctl00$cpMain$Country'] = rrContext.req.params.ctl00$cpMain$Country || 0;
        requestInfo.form['ctl00_cpMain_cpMain_Year'] = rrContext.req.params.ctl00_cpMain_cpMain_Year || '';

        Request.post(requestInfo, function (error, response, body) {

            rrContext.onResponse(error, response);

            if (rrContext.hadError()) {
                // TODO figure this out...
                callback(rrContext.getError(), rrContext, states);
                return;
            }

            var shows = UKI.parseHTML(body);
           global.nextq.mdb.addNewShows(shows, UKI.orgKey, ['All'], function(err, mergedShows) {
                callback(err, rrContext, states);
            });

           Utils.logWithDate('UKI brought up to date for states: ' + JSON.stringify(states));
        });
    }
};


module.exports = UKI;
