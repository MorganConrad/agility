
function FUN() {}

FUN.bringUpToDate = function(rrContext, states, callback) {
   "use strict";

   callback(null, rrContext, states);
};

module.exports = FUN;