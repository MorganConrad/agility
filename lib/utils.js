/**
 * Utilities
*/



function Utils() {}  // pretend "class" for easier exporting...

// general purpose

Utils.minuteToMS = Number(60*1000);

Utils.allowCachedShows = false;  // change to false, mainly in development mode,  to force updates for every request

/**
 * Convert a date into an 8 digit integer  YYMMDD
 * @param date
 * @returns {number}  YYMMDD
 */
Utils.dateNum = function(date) {
    "use strict";
    return (date) ?
       (date.getFullYear()-2000)*10000 + (date.getMonth()+1)*100 + date.getDate() : 0;
};


Utils.cleanupLocation = function(str) {
    "use strict";
    str = str.replace(/\s+/g, ' ');  // multiple spaces -> single space
    str = str.replace(' ,', ',');    // delete extraneous space before a comma
    return str.trim();
};

/**
 * Calls fnkv(key, value) for every enumerable key in obj
 * 
 * @param obj
 * @param fnkv
 * @param inherit  if true, also inherit keys.  optional, default is false
 */
Utils.eachKV = function(obj, fnkv, inherit) {
    "use strict";
    for (var key in obj)
        if (inherit || obj.hasOwnProperty(key))
            fnkv(key, obj[key]);
};


/**
 * Returns all the values of an object
 * @param obj
 * @param inherit  if true, also inherit values.  optional, default is false
 * @returns {Array}
 */
Utils.objectValues = function(obj, inherit) {
    "use strict"; 
    var arr = [];
    for (var key in obj)
        if (inherit || obj.hasOwnProperty(key))
            arr.push(obj[key]);
    
    return arr;
};


// may return a negative number
Utils.hashCode = function(str) {
   "use strict";
   var hash = Number(0);
   for (var i=0; i<str.length; i++) {
      hash = (hash*33 + str.charCodeAt(i)) << 0; 
   }
   return hash;
};


/**
 * Split a String and trim every part, deleting any that are blank
 * @param string
 * @param delim
 * @returns {Array}
 */
Utils.splitAndTrim = function(string, delim) {
    "use strict";
    var splits = string.split(delim);
    var snt = [];
    for (var i=0; i<splits.length; i++) {
        var trimmed = splits[i].trim();
        if (trimmed.length > 0)
            snt.push(trimmed);
    }

    return snt;
};


/* deprecated 3-8-14
Utils.binSearch = function(array, grail) {
    "use strict";
    var minIndex = 0;
    var maxIndex = array.length - 1;
    var currentIndex;
    var currentElement;

    while (minIndex <= maxIndex) {
        currentIndex = Math.floor((minIndex + maxIndex) / 2);
        currentElement = array[currentIndex];

        if (currentElement < grail) {
            minIndex = currentIndex + 1;
        }
        else if (currentElement > grail) {
            maxIndex = currentIndex - 1;
        }
        else {
            return currentIndex;
        }
    }

    return -1;
};
*/

/**
 * Convert o into an array unless it already is one
 * @param o
 * @returns []
 */
Utils.toArray = function(o) {
    "use strict";
   if ((o === null) || (o === undefined))
      return [];
   if (Array.isArray(o))
      return o;
   return Array.prototype.slice.call(arguments);
};


/**
 * Copies key/value pairs from -> to
 * @param from if null, returns to
 * @param to   if null, creates a new {}
 * @returns to, always an {} 
 */
Utils.copy = function(from, to, which) {
   "use strict";
   if (!to)
      to = {};

   if (from && (typeof from === 'object'))
      which = which || Object.keys(from);
      which.forEach(function (k) {
         to[k] = from[k];
      });

   return to;
};


Utils.logWithDate = function(message) {
   "use strict";
   console.log(new Date().toISOString() + ': ' + message);
};


// currently unused, taken from http://therockncoder.blogspot.com/2013/11/mobile-device-detection-and-redirection.html
Utils.isMobilePhone = function(req) {
   "use strict";
   var ua = req.headers['user-agent'].toLowerCase(),
      isMobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(ua) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(ua.substr(0, 4));

   return !!isMobile;
};


// more Dog Show related

/**
 * Return State code at end of a location
 * @param location  e.g. 'Boston MA'
 * @returns uppercase String or null
 */
Utils.getState = function (location) {
   "use strict";
   var idx = Math.max(location.lastIndexOf(','), location.lastIndexOf(' '), location.lastIndexOf('-'));
   if (idx >= 0) {
      var state = location.substr(idx + 1).trim();
      if (state.length >=2) {
         var cc = state.charCodeAt(0);
         if (cc > 64 && cc <= 90)  // A..Z
            return state;
      }
   }

   return null;  // not a state
};

// incoming is '["US-AL","CA-AB"]'
Utils.trimStates = function(statelist) {
   "use strict";
    if (!statelist)
       return [];
    
    if (statelist.charAt(0) === '[')
        statelist = statelist.substr(1, statelist.length - 2);
    
   var split = statelist.split(',');
   var trimmed = [];
   split.forEach(function(s) {
       var hyphen = s.indexOf('-');
       if (hyphen > 0)
          s = s.substring(hyphen+1, hyphen+3);
       
       trimmed.push(s);
   });
    
   return trimmed;
};


/**
 * Finds children[idx].data of the element
 * @param element
 * @param idx varargs, indices of children to drill down
 * @returns .data or null
 */
Utils.childData = function(element, idx) {
   "use strict";
   var child = element;
   for (var a=1; a<arguments.length && child && child.children; a++) {
      child = child.children[arguments[a]];
   }
   
   return child ? child.data : null;
};


/**
 * Finds children[idx].data of the element, and trims the result
 * @param element
 * @param idx
 * @returns String, '' if there was a problem
 */
Utils.childDataTrimmed = function(element, idx) {
   "use strict";
   var cd = Utils.childData.apply(this, arguments);
   return cd ? cd.trim() : '';
};


Utils.contigDates = function(date1, date2) {
    "use strict";
    var dates = [];
    var stop = date2 ? date2.getTime() : date1.getTime();

    // bug #7: check for year wrap around
    if (stop < date1.getTime()) {
        date2.setUTCFullYear(date2.getUTCFullYear()+1);
        stop = date2.getTime();
    }
    stop += Utils.minuteToMS*60*2.0; // add two hours

    var date = new Date(date1.getTime());
    while (date.getTime() < stop) {
        dates.push(new Date(date.getTime()));
        date.setDate(date.getDate() -0 + 1);
    }
    
    return dates;    
};



Utils.nicerDateString = function(date) {
    "use strict";
   date = date || new Date();
    var ugly = date.toDateString();
    return ugly.substr(4) + ' (' + ugly.substring(0, 3) + ')';
};


Utils.fixLink = function(link) {
    "use strict";
    if (link &&  'http' !== link.substr(0,4))
       return 'http://' + link;
    return link;
};




/* deprecated 3-8-14
Utils.uniques = function(fnUnique, arraysIn) {
    "use strict";
    var set = {};
    var i,k;
    for (var a=1; a<arguments.length; a++) {
       var arg = arguments[a];
       if (Array.isArray(arg)) {
           for (i=0; i<arg.length; i++) {
               var e = arg[i];
               set[fnUnique(e)] = e;
           }
       }
       else if (arg === Object(arg)) {
           for (k in Object.keys(arg))
             set[k] = arg[k];
       }
       else {
           var s = arg.toString();
           set[s] = s;
       }
    }
    
    var keys = Object.keys(set);
    var result = new Array(keys.length);
    for (i=0; i<keys.length; i++)
      result[i] = set[keys[i]];
    
    return result;
};
*/


Utils.validDateParse = function(inStr) {
    "use strict";
    var timestamp = Date.parse(inStr);
    return isNaN(timestamp) ? null : new Date(timestamp);
};


Utils.datePlusMonths = function(inDate, months) {
    "use strict";
    inDate = inDate || new Date();
    var date = new Date(inDate.getTime());
    date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    date.setMonth(date.getMonth() + months);
    return date;  // fix bug#2
    // return date.toJSON().substr(0,10);  wrong,left here in case I want to trigger test bugs...
};


Utils.datesWithin = function(date1, date2, days) {
   "use strict";
   var maxMS = (days * 24 + 2) * 60 * Utils.minuteToMS;  // +2 hours in case of saving time
   var diff = Math.abs(date1.getTime() - date2.getTime());
   return diff < maxMS;
};

/* deprecated 3-8-14
Utils.autoCSVFields = function(object, options) {
    "use strict";
    options = options || {};
    
    var autocapFirst = options.autocapFirst;
    
    var fields = [];
    Object.keys(object).forEach(function(key) {
        var format= {
            name: key,
            label: autocapFirst ? key.charAt(0).toUpperCase() + key.slice(1) : key
        };
        fields.push(format);
    });
    
    return fields;
};
*/


module.exports = Utils;