/**
 * Request uses qs to format forms, which means that multiple entries come out with indices, e.g.
 * foo[0]=bar0&foo[1]=bar1.
 * This little subclass uses querystring so the form data comes out as
 * foo=bar0&foo=bar1
 * 
 * @type {Request|exports}
 */

var Request = require('request/request.js');
var querystring = require('querystring');

MyRequest.prototype = Object.create(Request.prototype);
MyRequest.prototype.constructor = MyRequest;

function MyRequest(options, callbackfn) {
    "use strict";
    if (callbackfn)
       options.callback = callbackfn;
    options.method = options.method || 'POST';          // used currently only for posts
    Request.prototype.constructor.call(this, options);  // this will trigger everything, including the actual http request (icky)
}

// override hos form to use querystring
MyRequest.prototype.form = function (form) {
    "use strict";
    if (form) {
        this.setHeader('content-type', 'application/x-www-form-urlencoded; charset=utf-8');
        this.body = querystring.stringify(form).toString('utf8');
        return this;
    }
    
    else
       return Request.prototype.form.apply(this, arguments);
};


module.exports = MyRequest;
