var utils = require('./utils');
var Request = require('request');


/**
 * @param config   string or hash with appid, else uses process.env.YAHOO_APPID;
 * @constructor
 */
function GoogleGC(config, cache) {
   "use strict";
   if (typeof config === 'string') {
      config = { appid: config };
   }

   this.config = utils.copy(config);
   this.config.appid = this.config.appid || process.env.GOOGLE_APPID;

   this.cache = cache || {};
   this.newValues = [];
}


GoogleGC.prototype.getPlaceInfo = function (locationName, callback) {
   "use strict";
   if (locationName.length === 0)
      return callback(null, null);  // quietly return null, will get ignored

   locationName = utils.cleanupLocation(locationName);

   // Google doesn't like empty places

   // remove commas
   locationName = locationName.replace(',', '');
   //locationName = encodeURI(locationName);

   var state2 = utils.getState(locationName);

   var cached = this.cache[locationName];
   if (cached)
      callback(null, cached);

   else {
      var google = this;
      var url = "https://maps.googleapis.com/maps/api/geocode/json?key=" + this.config.appid;
      url = url + "&address=" + encodeURI(locationName);
      Request.get(url, function (error, response, body) {
         if (error)
            callback(error);
         else {
            var placeInfo = null;
            var fullResponse = JSON.parse(body);
            if (fullResponse.status !== 'OK')
               callback(fullResponse.status);
            var results = fullResponse.results ? fullResponse.results : [];
            if (results.length) {
               // just take the 1st one for now
               var result = results[0];
               placeInfo = {
                  _id: locationName,
                  path: GoogleGC.spaceToDash(result.formatted_address),
                  woeid: '',  // deprecated
                  longLat: [  // MongoDb prefers arrays for positions, IMPORTANT: long / lat order
                     result.geometry.location.lng,
                     result.geometry.location.lat
                  ]
               };

               google.cache[locationName] = placeInfo;
               google.newValues.push(placeInfo);
            }


            callback(error, placeInfo);  // note, placeInfo might be null...

         }
      });
   }
};


GoogleGC.prototype.setCache = function(values) {
   "use strict";
   if (Array.isArray(values)) {
      var google = this;
      values.forEach(function(v) {
         google.cache[v._id] = v;
      });
   }
   else
      this.cache = values || this.cache;
};


GoogleGC.prototype.getAndClearNewValues = function() {
   "use strict";
   var was = this.newValues;
   this.newValues = [];
   return was;
};

// static functions
GoogleGC.spaceToDash = function(inStr) {
   "use strict";
   return inStr.replace(/\s/g, '-');
};


/**
 * Calculate distance between two points, using Spherical Law of Cosines
 * @see http://www.movable-type.co.uk/scripts/latlong.html
 * @param lat1
 * @param lon1
 * @param lat2
 * @param lon2
 * @param R
 * @returns {number}
 */
GoogleGC.prototype.slocDistance = function(lat1, lon1, lat2, lon2, R) {
   "use strict";
   lat1 = GoogleGC.toRad(lat1);
   lat2 = GoogleGC.toRad(lat2);
   lon1 = GoogleGC.toRad(lon1);
   lon2 = GoogleGC.toRad(lon2);

   R = R || 3959.0;  // default is miles.  If you want Km, pass in 6371
   var temp = Math.acos(Math.sin(lat1)*Math.sin(lat2) +
      Math.cos(lat1)*Math.cos(lat2) * Math.cos(lon2-lon1));
   return temp * R;
};


GoogleGC.toRad = function(degrees) {
   "use strict";
   return degrees * Math.PI / 180;
};



module.exports = GoogleGC;
