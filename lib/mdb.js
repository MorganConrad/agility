/**
 * new improved mongo, keeps connection "open"
 */

var 
  MongoDB = require('mongodb'),
  Show = require('./show'),
  Utils = require('./utils'),
  Collector = require('./collector');


/**
 * Constructs and opens a mongo connection
 * 
 * @param uri
 * @param options   {
        connectOptions, uri, autosaveMinutes, refreshMinutes, errHandler
        }
 * @param geocoder    required, pointer to an instance of geocoder
 * @param callback optional
 * @constructor
 */
function MDB(uri, dbname, options, geocoder, callback) {
   "use strict";
   options = options || {};
   uri = uri || options.uri || options.url;
   if (!uri)
      throw "must have a uri";
   if (!dbname)
     throw "must have a dbname";
   if (!geocoder)
      throw "must have a geocoder";
   this.geocoder = geocoder;
   this.authors = {};  // perhaps move elsewhere...

   this._errHandler = options.errHandler || function(err) { if (err) console.error(err); };
   
   this.options = Utils.copy(options);
   this.options.uri = uri;
   this.connectOptions = this.options.connectOptions || {};
   this.options.autosaveMinutes = this.options.autosaveMinutes || 60*2;  // 2 hours
   this.options.refreshMinutes = this.options.refreshMinutes ||   60*24; // 2 days
   this.lastSaveStuffMS = +0;
   
   this._initLastUpdated();
   
   var mdb = this;
   MongoDB.MongoClient.connect(uri, options.connectOptions, function (err, client) {
      if (err) {
         if (client)
           client.close(true);
         // db = null;
         mdb._errHandler(err);
         // fall through
      }
      mdb._client = client;
      mdb._db = client.db(dbname);

      if (callback)
         callback(err);
   });
}



MDB.prototype.close = function() {
   "use strict";
   if (this._db)
      this._db.close();
   this._db = null;
};


MDB.prototype.db = function() {
   "use strict";
   return this._db;
};


MDB.prototype.startUp = function(callbackFn, callbackThis) {
   "use strict";
   var ids = Show.ALL_ORGS.map(function(w) {
      return 'lu_' + w;
   });
   var query = {_id: {$in: ids }};
   var mdb = this;

   var clt = new Collector(3, callbackFn, callbackThis);
   clt.finallyCall(function (ignored) {
         // should contain placeinfo, an array, and misc: [4 last updateds]
         mdb.geocoder.setCache(clt.results.placeinfo);
         mdb.addAuthors(clt.results.authors);
         clt.results.misc.forEach( function(o) {
            mdb.lastUpdated[o.org] = o;
         });
      });

   this._loadCollection('misc', query, {}, clt);
   this._loadCollection('authors', {}, {}, clt);
   this._loadCollection('placeinfo', {}, {}, clt);
};




MDB.prototype.loadCollections = function(whichCollections, querys, findOptions, callbackFn, callbackThis) {
   "use strict";
   if (!Array.isArray(whichCollections))
      whichCollections = [whichCollections];
   findOptions = findOptions || {};
   var mdb = this;

   var clt = new Collector(whichCollections.length, callbackFn, callbackThis);   
   for (var c=0; c<whichCollections.length; c++) {
      var theQuery = Array.isArray(querys) ? querys[c] : querys;
      mdb._loadCollection(whichCollections[c], theQuery, findOptions, clt);
   }

};

// could be optimized???
MDB.prototype.findOneShow = function(showID, callbackFn, callbackThis) {
   "use strict";
   var orgKey = Show.org(showID);
   var query = { _id: showID };   
   this.loadCollections(orgKey, query, {}, callbackFn, callbackThis);
};



MDB.prototype.findByIDs = function(showIDs, callbackFn, callbackThis) {
   "use strict";
   if (!Array.isArray(showIDs))
      showIDs = showIDs.split('+');
   
   var hash = {};
   
   showIDs.forEach(function(showID) {
      var orgKey = Show.org(showID);
      if (hash[orgKey])
         hash[orgKey].push(showID);
      else
         hash[orgKey] = [showID];
   });
   
   var whichCollections = [];
   var queries = [];
   for (var orgKey in hash) {
      whichCollections.push(orgKey);
      queries.push( {_id: { $in: hash[orgKey]}});
   }
   
   this.loadCollections(whichCollections, queries, {}, callbackFn, callbackThis);
};




MDB.prototype.addNewShows = function(newShows, orgKey, states, callback) {
   "use strict";
   var mdb = this;
   var date = new Date();

   var removeQuery = {};
   if (states.length  && states[0] !== 'All') {
      removeQuery = {state: {$in: states }};
   }

   // new step = add long and lat
   Show.addLatLon(global.nextq.geocoder, newShows, function(err) {
      if (err)
         callback(err);
      else {
         var showCount = newShows.length;
         var clt = new Collector(1, callback);
         mdb._withCollection(orgKey, clt, function(coll) {
            coll.deleteMany(removeQuery, function(err) {
               if (clt.progress(err, orgKey, "removed")) {
                  coll.insertMany(newShows, function(err) {
                     if (!showCount || clt.progress(err, orgKey, "added")) {  // ignore the Mongo error for no documents TODO fix better
                        mdb._updateAndSaveLastUpdated(orgKey, states, null, clt);
                     }
                  });
               }
            });
         });
      }
   });

};



MDB.prototype.insert = function(collectionName, data, callback) {
   "use strict";
   var mdb = this;
   var clt = new Collector(1, callback);
   mdb._withCollection(collectionName, clt, function(coll) {
      coll.insertMany(data, callback);
   });
};



//MDB.prototype.submitShow = function(show, callback) {
//   "use strict";
//   var mdb = this;
//   var clt = new Collector(1, callback);
//   mdb._withCollection(show.org, clt, function(coll) {
//      coll.insert(show, callback);
//   });
//};


MDB.prototype.buildQuery = function(req, states) {
   "use strict";
   states = ['**'].concat(states); // add wildcard
   var mquery = req.query['mquery'];
   if (mquery)
      return JSON.parse(mquery);

   var query = {};
   Show.commonKeys.forEach(function (key) {
      var q = req.query[key];
      if (q)
         query[key] = q;
   });

   // add dates
   var startDate = Utils.validDateParse(req.query.start);
   startDate = startDate || new Date();
   var endDate = Utils.validDateParse(req.query.end);
   if ((!endDate) || (endDate.getTime() <= startDate.getTime()) )
      endDate = Utils.datePlusMonths(startDate, 3);
   //  endDate.setDate(endDate.getDate() + 1);

   // use _id field cause it's indexed and I'm not sure how to query array[0]
   var idMin = Utils.dateNum(startDate) + '_';
   var idMax = Utils.dateNum(endDate) + 'z';  // z > _
   query._id = {$gte: idMin, $lt: idMax};

   if ('All' !== states[0]) {
      query['state'] = { $in: states };
   }

   var foo = JSON.stringify(query); // TEMP
   return query;
};



/* 
 Administrative functions, specific to my impl
 */
MDB.prototype.reset = function(more, callback, callbackThis) {
   "use strict";
   this._initLastUpdated();
   
   more = (more && more.length>0) ? more : 'misc';
   var mores = more.split(',');
   var collectionNames = [];
   for (var m=0; m<mores.length; m++) {
      if ('orgs' === mores[m])
         collectionNames = collectionNames.concat(Show.ALL_ORGS_PLUS_FUN_WS);
      else
         collectionNames.push(mores[m]);
   }
   
   this.removeCollections(collectionNames, {}, callback, callbackThis);
   
};



MDB.prototype.purgeOld = function(beforeDate, callbackFn, callbackThis) {
   "use strict";

   var minID = Utils.dateNum(beforeDate) + '_9';
   var query = { _id : { $lt : minID }};

   this.removeCollections(Show.ALL_ORGS_PLUS_FUN_WS, query, callbackFn, callbackThis);
};


MDB.prototype.removeCollections = function(collectionNames, query, callbackFn, callbackThis) {
   "use strict";
   collectionNames = Utils.toArray(collectionNames);
   var clt = new Collector(collectionNames.length, callbackFn, callbackThis);
   query = query || {};
   var mdb = this;
   collectionNames.forEach(function (collectionName) {
      mdb._withCollection(collectionName, clt, function (coll) {
         coll.deleteMany(query, function (err) {
            clt.collect(err, collectionName, "removed");
         });
      });
   });
};


MDB.prototype.saveStuff = function(callbackFn, callbackThis) {
   "use strict";
   var mdb = this;

   if (callbackFn || (Date.now() - this.lastSaveStuffMS) > Utils.minuteToMS*this.options.autosaveMinutes) {
      this.lastSaveStuffMS = Date.now();

      callbackFn = callbackFn || mdb._errHandler; // if no callback, at least track errors
      var clt = new Collector(2, callbackFn, callbackThis);
      this._bulkInsert('placeinfo', mdb.geocoder.getAndClearNewValues(), {continueOnError: true}, clt);
      this._replaceCollection('misc', Utils.objectValues(mdb.lastUpdated), {}, clt);
      Utils.logWithDate('MDB.saveStuff completed ');
   }
};



MDB.prototype.getLastUpdated = function(org, st) {
   "use strict";
   var forOrg = this.lastUpdated[org];
   if (forOrg)
      return forOrg.state[st] || new Date(0);
   else
      return new Date(0);
};


MDB.prototype.getOutOfDates = function(org, states, minutes) {
   "use strict";
   var outOfDates = [];
   minutes = minutes || this.options.refreshMinutes;
   var nowMS = Date.now();
   for (var i=0; i<states.length; i++) {
      var cacheMS = this.getLastUpdated(org, states[i]).getTime();
      if ( (nowMS - cacheMS) > Utils.minuteToMS*minutes)
         outOfDates.push(states[i]);
   }

   return outOfDates;
};


/*
"private" methods hitting the db require a collector, not a callback
*/
MDB.prototype._withCollection = function(collection, collector, callback) {
   "use strict";
   if (typeof collection === 'string'){
      this._db.collection(collection, function (err, coll) {
         if (collector.progress(err, collection))
            callback(coll);
      });
   }
   else if (collection.collectionName)  // it is already a collection
      callback(collection);
   else
      throw "not a string or collection";
};


MDB.prototype._bulkInsert = function( collection, docs, options, collector) {
   "use strict";
   options = options || {};
   if (docs.length)
      this._withCollection(collection, collector, function(coll) {
         coll.insertMany(docs, options, function(err) {
            collector.collect(err, coll.collectionName, docs);
         });
      });
   else {
      var name = collection.collectionName || collection;
      collector.collect(null, name, docs);
   }     
};


MDB.prototype._loadCollection = function (collection, query, findOptions, collector) {
   "use strict";
   this._withCollection(collection, collector, function (coll) {
      coll.find(query, findOptions).toArray(function (err, items) {
         collector.collect(err, coll.collectionName, items);
      });
   });


};


MDB.prototype._replaceCollection = function(collection, docs, options, collector) {
   "use strict";
   var mdb = this;
   this._withCollection(collection, collector, function(coll) {
      coll.deleteMany(function (err) {
      if (collector.progress(err, coll.collectionName))
         mdb._bulkInsert(coll, docs, options, collector);
      });
   });
};



MDB.prototype._updateAndSaveLastUpdated = function(org, states, date, collector) {
   "use strict";
   states = Utils.toArray(states || 'All');
   date = date || new Date();

   var set = { $set: { org: org} };
   var lupdatedorgstates = this.lastUpdated[org].state;
   states.forEach(function(st) {
      lupdatedorgstates[st] = date;
      set.$set['state.' + st] = date;
   });

   this._withCollection('misc', collector, function (coll) {
      coll.updateMany({_id: 'lu_' + org}, set, {upsert: true}, function (err, result) {
         collector.collect(err, org, result);
      });
   });
};



MDB.prototype._initLastUpdated = function(orgs) {
   "use strict";
   orgs = orgs || Show.ALL_ORGS;
   this.lastUpdated = {};
   var lu = this.lastUpdated;
   orgs.forEach(function(k) {
      lu[k] = { _id: 'lu_' + k, org: k, state: {}};
   });
};


MDB.prototype.addAuthors = function(values) {
   "use strict";
   if (Array.isArray(values)) {
      var mdb = this;
      values.forEach(function(v) {
         mdb.authors[v._id] = v.author;
      });
   }
};


module.exports = MDB;

