var utils = require('./utils');
var Collector = require('./collector');

function Show(org) {
   "use strict";
   if (!(this instanceof Show))
      return new Show(org);
    
   this.org = org;   
}

Show.ALL_ORGS = ['akc', 'cpe', 'nadac', 'uki', 'usdaa'];
Show.ALL_ORGS_PLUS_FUN_WS = ['akc', 'cpe', 'nadac', 'uki', 'usdaa', 'fun', 'work'];
Show.UNKNOWN_DATE = new Date('Mar 27 2006'); // Boomer's birthday

Show.GEOCODER = null;

Show.prototype.id = function() {
    "use strict";
    if ((this._id === null) || (this._id === undefined)) {
        this._id = Show.genId(this);
    }
    
    return this._id;
};


Show.prototype.toString = function() {
    "use strict";
    return JSON.stringify(this, null, 2);
};

Show.commonKeys = ['club', 'location', 'day', 'date', 'state'];// TODO add url/email but need to standardize


/**
 * Do some shortening and cleaning up - return a URL legal ID
 * @param show
 * @returns YYMMDDLOSSHcluAb  
 * where L = length in days (mod 10), O is the organization and SS is the state, H a hash
 * club may include some capitalized common abbreviations, e.g. D = dog
 */
Show.genId = function(show) {
    "use strict";
    var d0 = utils.dateNum(show.dates[0]);
    var len = show.dates.length % 10;  // length of the show in days
   
   // get rid of spaces and all non-URLy chars, convert to lowercase
    var hashcode = utils.hashCode(show.club) & 31;  // avoid accidental equals after all the cleanup
    var hashchar = hashcode > 9 ? String.fromCharCode(65+hashcode-10) : String.fromCharCode(48+hashcode);
    var club = show.club.replace(/\W/g, '').toLowerCase();
    for (var key in abbreviations)
       club = club.replace(key, abbreviations[key]);
   
    return '' + d0 + len + Show.orgToChar(show.org) + show.state + hashchar + club;
};


Show.jsonReplacer = function(key, value) {
    "use strict";
    return (key === '_id') ? undefined : value;
};


/* deprecated 3-8-14
Show.mergefn = function(newShows, prevShows) {
    "use strict";
    newShows.forEach(function (show) {
        show.id();  // make sure every new show has it's id generated...
    });
    // paranoia / cleanup - set old shows too
    prevShows.forEach(function (show) {
        if (show._id === undefined)
            show._id = Show.genId(show);
    });
   return utils.uniques(function(show) {return show._id;}, newShows, prevShows);
};
*/

Show.createDays = function(dateArray) {
    "use strict";
    if (dateArray.length === 1)
       return utils.nicerDateString(dateArray[0]);
    
    var d0 = dateArray[0];
    var d1 = dateArray[dateArray.length-1];
    
    // if month or year differ, not much we can do
    if ((d0.getMonth() !== d1.getMonth()) ||
        (d0.getYear() !== d1.getYear()))
       return utils.nicerDateString(d0) + ' - ' + utils.nicerDateString(d1);
    
    // same month and year, let's shorten it up   
    var s0 = d0.toDateString();  // e.g. Sat Jan 24 2014
    var s1 = d1.toDateString();  // e.g. Mon Jan 26 2014
    
    return s0.substr(4,6) + // Month and 1st day
           '-' + s1.substr(8,7) + // 2nd day and year
           ' (' + s0.substr(0,3) + '-' + s1.substr(0,3) + ')';  // add days
    
};


Show.cleanup = function(show) {
    "use strict";    
    if (!show.dates || !show.dates.length) {
        console.log("Show without a date, club= " + show.club);
        show.dates = [Show.UNKNOWN_DATE];  // at least one exists
        show.days = 'TBA';
       // temp fix bug 7 - TBA for their Nationals as a location screws up google locations...
       return null;
    }
    else
      show.days = Show.createDays(show.dates);
    
    show.location = utils.cleanupLocation(show.location);  // multiple spaces -> single space
    show.club = utils.cleanupLocation(show.club);  // multiple spaces -> single space    
    show._id = Show.genId(show);

    // TODO more here???  (lat & lon?)
    
   // bug #4  remove any null, empty etc... urls
    show.urls = show.urls.filter(function(url) { return url;} ) ;
    
    return show;
};


// async so split off...
Show.addLatLon = function(geocoder, showlist, callback) {
    "use strict";
    
    if (!showlist.length) {
        callback();
        return;
    }
    
    var clt = new Collector(showlist.length, callback);
    
    showlist.forEach(function(show) {
        if (show.longLat)  // already there
           clt.collect();
        else
           geocoder.getPlaceInfo(show.location, function(error, placeInfo) {
               if (placeInfo)
                   show.longLat = placeInfo.longLat;
               // temp, ignore errors cause geocoding is flaky
                error = null;
                clt.collect(error);
           });
    });
};


// incoming is 1404191ACAQSerncaliforniaportuguesewaterDC
Show.org = function(showID) {
   "use strict";
   return Show.charToOrg[showID.charAt(7)];
};


Show.distanceFrom = function(lat, lon, show) {
    "use strict";
    show = show || this;
    
    if (show.longLat && lat < 100.0) 
       return Show.GEOCODER.slocDistance(lat, lon, 
                                       show.longLat[1], show.longLat[0]);  // note order switch
    else
       return Number.NaN;  
};

module.exports = Show;


Show.charToOrg = {
   A : 'akc',
   C : 'cpe',
   I : 'uki',
   N : 'nadac',
   U : 'usdaa'
};


Show.orgToChar = function(org) {
   "use strict";
   if (('uki' === org) || ('UKI' === org))
      return 'I';
   else
      return org.charAt(0).toUpperCase();
};


var abbreviations = {  
   agility: 'A',
   club: 'C',
   dog: 'D',
   east: 'E',
   fanciers: 'F',
   inc: 'I',
   kennel: 'K',
   collie: 'L',
   north: 'N',
   obedience: 'O',
   spaniel : 'P',
   retriever : 'R',
   south: 'S',
   training: 'T',
   america: 'U',
   west: 'W',
   canada: 'Z'
};