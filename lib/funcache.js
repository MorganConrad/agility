/**
 * Created by Morgan on 2/4/2016.
 */


/**
 * Constructor
 *
 * @param fn        your long running function, 2 args, the data and a callback:  fn(fnData, function (err, value, ttl))
 * @param fnData    data for your function, whatever format you like, often null
 * @param options   { "ttl" : timeToLiveInSeconds }  (default = 10)
 * @returns {Funcache}
 * @constructor
 */
function Funcache(fn, fnData, options) {
    "use strict";

    if (!(this instanceof Funcache))
        return new Funcache(fn, fnData, options);

    this.options = options || {};
    this.fn = fn;
    this.fnData = fnData;

    this._expires = 0;
}

Funcache.DEFAULT_TTL = 60 * 10;  // 10 minutes()

function nowSecs() {
    return Date.now() / 1000;
}


/**
 * Manually set the value, typically clients will *not* call this
 *
 * @param value
 * @param ttl     in seconds, defaults to this.options.ttl or Funcache.DEFAULT_TTL
 */
Funcache.prototype.set = function (value, ttl) {
    this._value = value;
    ttl = ttl || this.options.ttl || Funcache.DEFAULT_TTL;
    this._expires = nowSecs() + ttl;
}


/**
 * Get the value
 *   If callback is null, synchronous, returns value or null if expired.  Not recommended
 *
 * @param callback   a function(err, value)
 * @returns {*}
 */
Funcache.prototype.get = function (data, callback) {
    var now = nowSecs();

    if (!callback)  // synchronous mode
       return (now < this._expires) ? this._value : null;

    if (now < this._expires) {
        return callback(null, this._value);
    }

    else if (this._expires < 0) {  // in progress, add ourselves to be called when done
        this._callbacks.push(callback);
        return;
    }

    // normal flow, refresh _value using this.fn()

    this._callbacks = [callback];
    this._expires = -1;  // flags in progress
    var self = this;
    data = data || this.fnData;
    this.fn(data, function (err, value, ttl) {
        if (!err)
           self.set(value, ttl);

        // now call all the callbacks
        self._callbacks.forEach(function(cb) {
            cb(err, self._value);  // send prev value on an error
        })
        return;
    });
}


module.exports = Funcache;