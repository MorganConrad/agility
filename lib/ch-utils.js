
/**
 * Utilities specific to Cheerio
 */


const Cheerio = require("cheerio");


function CHUtils() {}


/**
 * Finds all <a href="xxx">s within an element, returning an array of the xxxs
 *
 * @param cheerio  a cheerio object, or gets converted to one
 * @param prepend  gets prepended to each value, e.g. a root URL, default = ''
 * @returns {Array} may be empty, usually only contains one
 */
CHUtils.findAHRefs = function(elem, prepend) {
   "use strict";
   if (!elem.cheerio)
      elem = Cheerio(elem);
   prepend = prepend || '';
   var as = elem.find('a');
   var hrefs = [];
   for (var i = 0; i < as.length; i++) {
      var href = as[i].attribs.href;
      if (href)
         hrefs.push(prepend + href);
   }

   return hrefs;
};


/**
 * Retrieves trimmed text from an element/tag using Cheerio
 * @param elem : CO or a PODO
 * @returns {*|String}
 */
CHUtils.text = function(elem) {
   "use strict";
   if (!elem.length)  // it is a raw PODO
      elem = elem.children;

   return Cheerio.text(elem).trim();
};



module.exports = CHUtils;

