/**
 * Holds info about the original Request/Response (from the user)
 * plus the error and status code from the triggered mikeal request
 * @author Morgan Conrad
 * Copyright(c) 2013
 */
var Show = require('./show');
var csv = require('./myjson-csv');
var Utils = require('./utils');
var ICS = require('./ics');


function RRContext(req, res, jadePage, jsonFilter) {
    "use strict";
    this.req = req;
    this.res = res;
    this.jadePage = jadePage;
    this.time = new Date();
    this.jsonFilter = jsonFilter || Show.jsonReplacer;
    
    this.longLat = [500.0, 500.0];
   
   // for POST, copy any body parameters to the query
   Utils.copy(req.body, req.query);
}


RRContext.MIME_TYPES = {
    csv:  'text/csv',
    html: 'text/html',
    json: 'application/json',
    jsonp: 'application/javascript',
    xml:  'application/xml'  // TODO
};

/**
 * Call when mikealrequest returns
 */
RRContext.prototype.onResponse = function(error, response) {
    "use strict";
    this.error = error;
	if (response) {
       this.statusCode = response.statusCode;
       this.followupUrl = response.request.href;
      
      if (response.statusCode >= 400) {
         this.error = {
            statusCode : response.statusCode,
            message : response.body
         };
      }
	}
	else {
	   this.statusCode = 500;
	}  
};

RRContext.prototype.onCached = function(followupUrl, cacheDate) {
    "use strict";
    this.error = null;
    this.statusCode = 200;
    this.followupUrl = followupUrl;
    this.cacheDate = cacheDate;
};


RRContext.prototype.hadError = function () {
    "use strict";
    return (this.error || this.statusCode !== 200);
};


RRContext.prototype.getError = function () {
   "use strict";
   return (this.error);
};


RRContext.prototype.olddecideMIMEFormat = function(req) {
   "use strict";
   req = req || this.req;
   
   // check body of POST (for .csv)  NOTE: I think the POST and params.format stuff is obsolete but keeping for now...
   var format = req.body.format;
   if (!format)  // look for jsonp or format parameter
     format = req.params.jsonp ? 'jsonp' : this.req.params.format;
   // new for 1.0.0 Express is slightly different...
   if (!format)
      format = req.query.format;

   if (!format && req.url.substr(0, 3) === '/v1')  // v1 REST defaults to json
      format = 'json';
   if (!format)
      format = this.cannedContentType;
   if (format)
      return format;

   // pick html or json depending on which is first in the accept headers
   var accept = req.headers.accept || 'html';  // bug#5 default to 'html'
   var ihtml = accept.indexOf('html');
   var ijson = accept.indexOf('json');
   if (ihtml < 0) ihtml = accept.length;
   if (ijson < 0) ijson = accept.length;
   return (ihtml <= ijson) ? 'html' : 'json';
};


RRContext.prototype.decideMIMEFormat = function(req) {
   "use strict";
   req = req || this.req;

   if (req.query.jsonp !== undefined)
      return 'jsonp';

   var format = req.query.format;

   if (!format && req.url.substr(0, 3) === '/v1')  // v1 REST defaults to json
      format = 'json';
   if (!format)
      format = this.cannedContentType;  // legacy for tests
   if (format)
      return format;

   // no format defined in query, pick html or json depending on which is first in the accept headers
   var accept = req.headers.accept || 'html';  // bug#5 default to 'html'
   var ihtml = accept.indexOf('html');
   var ijson = accept.indexOf('json');
   if (ihtml < 0) ihtml = accept.length;
   if (ijson < 0) ijson = accept.length;
   return (ihtml <= ijson) ? 'html' : 'json';
};


/**
 * Sends data to original response, adding meta
 * checks for jsonp
 * @param name
 * @param data
 */
RRContext.prototype.send = function(name, data, format) {
    "use strict";
    
   // special check for shutdowns
   if (!this.res) {
      // console.log("Command Request: %j", this.req);
      console.log("Response: %j", data);
      return;
   }
   
    format = format || this.decideMIMEFormat();
    var functionJSONP = null;
    if ('jsonp' == format)
       functionJSONP = this.req.query.jsonp || 'callback';
   
    var rr = this;

    var lat = this.longLat[1];
    var lon = this.longLat[0];
    var distancefn = function(show) {
        var distance = Show.distanceFrom(lat, lon, show);
        if (Number.isNaN(distance))
           return ' ';
        return Math.ceil(distance);
    };
    
   var nearText = this.near ? ' near ' + this.near : '';
   this.res.statusCode = this.statusCode || 200;
   
    if ('html' === format) {
       if (this.res.statusCode < 400) {
        this.res.render(this.jadePage, 
           { shows: data, 
             nearText : nearText,
             today: Utils.dateNum(new Date()), 
             origUrl: this.req.url,
             againURL: this.mobile ? '/m.html' : '/index.html',
             fns: { 
                 distance: distancefn
                  },
               title: 'Upcoming Trials ' + nearText
           } );
       }
       else {
          var jadePage;
          switch(this.res.statusCode) {
             case 400: jadePage = '400'; break;  // TODO ick
             case 403: jadePage = '403'; break;
             case 404: jadePage = '404'; break;
             default: jadePage = '50x';
          }
          this.res.render(jadePage, { error: this.error } );
       }
       
        return;  // DONE
    }

    // move later for 1.0.0, cant be null...  this.res.setHeader('content-type', RRContext.MIME_TYPES[format]);
    // this.res.statusCode = this.statusCode || 200;

    if ('csv' === format) {
       this.saveCSV(data);
       return;
    } 
    else if ('ics' === format) {
       this.saveICS(data);
       return;
    }

    // at this point, we assume JSON.  Just in case they ask for something goofy, make it so
    var mimeType = RRContext.MIME_TYPES[format];
    if (!mimeType) {
       mimeType = RRContext.MIME_TYPES.json;
       this.error = "Warning, " + format + " is not a valid format, using json instead";
    }
    this.res.setHeader('content-type', mimeType);
    this.res.statusCode = this.statusCode || 200;

    var response = {
        meta: {
            time: this.time,
            statusCode: this.statusCode,
            error: this.error,
            originalUrl: this.req.originalUrl,
            followupUrl: this.followupUrl,
            cacheDate: this.cacheDate
        }
    };
    
    if (name && data) // add in "real" data
      response[name] = data;

    var asString = JSON.stringify(response, this.jsonFilter, 2);
    if (functionJSONP) {
        // TODO simple test that functionJSONP is a legal id
        if (/\W/.test(functionJSONP)) {
            response.meta.error = 'illegal characters in jsonp: ' + functionJSONP;
            asString = JSON.stringify(response, undefined, 2);
            this.res.setHeader('content-type', RRContext.MIME_TYPES.json);  // change back...???
            this.res.send(asString);
        }
        else {
           this.res.send(functionJSONP + '(' + asString + ');');
        }
    } 
    
    else  // JSON
       this.res.send(asString);
};



RRContext.prototype.saveCSV = function (data, filename) {
    "use strict";

    var lat = this.longLat[1];
    var lon = this.longLat[0];
    var distancefn = function(show) {
        if (show.longLat)
            return Math.ceil(Show.distanceFrom(lat, lon, show));
        return -1;
    };
    
    var fields = [
        { name: 'org', label: 'Org'},
        { name: 'location', label: 'Location'},
        { name: 'days', label: 'Date'},
        { name: 'club', label: 'Club'},
        { name: null, label: 'Miles', filter: distancefn },
        { name: 'urls', label: 'Link(s)'}
    ];

    var rr = this;
    csv.toCSV( { data: data, fields : fields }, function(err, csv) {
        if (err)
            rr.res.sendError(err, 500);
        else {
            filename = filename || 'search';
            if (rr.near)
                filename = filename + '_' + rr.near;
            rr.res.attachment(filename + '.csv');
            rr.res.send(csv);
        }
    });
};


RRContext.prototype.saveICS = function (data, filename) {
   "use strict";
   
   var ics = new ICS( {
      allEventsAreAllDay: true,
      domain: "nextq.info",
      PRODID: "-//FlyingSpaniel//nextq.info//EN"}
   );
   

   for (var i=0; i<data.length; i++) {
      var show = data[i];
      var xformed = {
         allDay: true,
         start: show.dates[0],
         end:   show.dates[show.dates.length-1],
         uid:   show._id + '@nextq.info',
         description: show.org.toUpperCase() + '  ' + show.club + ' at ' + show.location,
         location: show.location,
         summary: show.club,
         url: show.urls.length ? show.urls[0] : null
      };
      ics.addEvents(xformed);
   }
   
   var bigString = ics.toICS(['LOCATION','DESCRIPTION', 'URL']);

   var rr = this;

   filename = filename || 'search';
   if (rr.near)
      filename = filename + '_' + rr.near;
   rr.res.attachment(filename + '.ics');
   rr.res.send(bigString);
   
};
   




RRContext.prototype.setLongLat = function(longLat) {
    "use strict";
    this.longLat = longLat;
};


RRContext.prototype.sendError = function (error, statusCode) {
   "use strict";
   statusCode = statusCode || 500;
   
   var message = '';
   
   // convert error to a string
   if (typeof error === 'string')
      message = error;
   else Utils.eachKV(error, function(k,v) {
      message += 'Trying to contact ' + k + ' produced an error: ' + JSON.stringify(v);
   });
   
   this.error = {
      errorcode: statusCode, 
      message: message
   };
   this.statusCode = statusCode ;
   this.send(null, null);
};

module.exports = RRContext;