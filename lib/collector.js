/**
 * Poor man's async
 * */


/**
 * 
 * @param count
 * @param onCompleteCallbackFn
 * @param onCompleteThis
 * @constructor
 */
function Collector(count, onCompleteCallbackFn, onCompleteThis) {
   "use strict";
   this.count = count;
   this._onComplete = {
      callbackFn: onCompleteCallbackFn,
      callbackThis: onCompleteThis
   };
   this._finally = {};
   this.results = {};
   this.errors = null;
}


Collector.prototype.failFast = function (failFastFn) {
   "use strict";
   this.failFastFn = failFastFn;
};


Collector.prototype.onComplete = function (callbackFn, callbackThis) {
   "use strict";
   this._onComplete.callbackFn = callbackFn;
   this._onComplete.callbackThis = callbackThis || this;
   return this;
};


Collector.prototype.finallyCall = function (fn, fnThis, evenIfErrors) {
   "use strict";
   this._finally._call = {
      fn: fn,
      fnThis: fnThis || this,
      evenIfErrors: evenIfErrors || false  // default to false
   };
   return this;
};

Collector.prototype.noErrors = function () {
   "use strict";
   return !this.errors;
};


Collector.prototype.finallyClose = function (closeMes) {
   "use strict";
   if (!this._finally.close)
      this._finally.close = [];

   this._finally.close = this._finally.close.concat(Array.prototype.slice.call(arguments));
   return this;
};


Collector.prototype.finallySet = function (object, resultsKey, errorsKey) {
   "use strict";
   this._finally.set = {
      object: object,
      resultsKey: resultsKey,
      errorsKey: errorsKey
   };

   return this;
};


Collector.prototype.progress = function (err, key) {
   "use strict";
   if (!err)
      return true;

   key = key || '_unknownStep_';
   this.count--;

   if (!this.errors)
      this.errors = {};

   this.errors[key] = err;
   if (this.failFastFn)
      this.failFastFn(err);
   else if (this.count <= 0) {
      forceDone(this);
   }
   
   return false;
};


Collector.prototype.collect = function (err, key, data) {
   "use strict";
   if (this.count <= 0) {
      //throw "called collect with count =" + this.count;
      console.log("warning, called collect with count =" + this.count);
      return; // don't forceDone twice!!!
   }

   if (this.progress(err, key)) {
      if (key)
         this.results[key] = data || 'OK';

      if (--this.count <= 0) {
         forceDone(this);
      }
   }
};



function forceDone(collector) {
   "use strict";
   collector.count = 0;
   collector._doFinally();

   if (collector._onComplete.callbackFn)
      collector._onComplete.callbackFn.call(collector._onComplete.callbackThis, collector.errors, collector.results);
}


Collector.prototype._doFinally = function () {
   "use strict";

   if (this._finally.close) {
      this._finally.close.forEach(function (c) {
         c.close();
      });
   }

   if (this._finally.set) {
      var obj = this._finally.set.object;
      var errorsKey = this._finally.set.errorsKey;
      if (errorsKey)
         obj[errorsKey] = this.errors;
      var resultsKey = this._finally.set.resultsKey;
      if (resultsKey)
         obj[resultsKey] = this.results;
   }

   if (this._finally._call) {
      if (this._finally._call.evenIfErrors || this.noErrors())
         this._finally._call.fn.call(this._finally._call.fnThis);
   }
};


module.exports = Collector;