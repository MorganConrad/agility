/**
 * @author Morgan Conrad
 * Copyright(c) 2014
 * Some code and conventions inspired by ical-generator  (https://www.npmjs.org/package/ical-generator)
 * This software is released under the MIT license  (http://opensource.org/licenses/MIT)
 */


function ICS(options) {
   "use strict";
   this.options = options || {};
   this.x_tensions = {};
   this.events = [];
}



function _escape(val) {
   "use strict";
   if (val === undefined)
      return '';
   
   if (typeof val === 'string' || val instanceof String)
      return val.replace(/[\\;,\n]/g, function (match) {
         return (match === '\n') ?'\\n': '\\' + match;
      });
   
   return val;
}


function _optVal(event, keyUC) {
   "use strict";
   var keyLC = keyUC.toLowerCase();
   var v = event[keyUC] || event[keyLC];
   if (v)
      return keyUC + ':' + _escape(v) + '\n';
   else
      return '';
}


function datestr(date, allDay, noLead) {
   "use strict";
   var str = date.toISOString();
   str = str.replace(/[^\d]/g, '');
   
   if (allDay)
      str = str.substr(0, 8);
   else
      str = str.substr(0, 8) + 'T' + str.substr(8) + 'Z';
   
   if (allDay)
      return noLead? str : 'VALUE=DATE:' + str;
   else
      return noLead? str : 'VALUE=DATE-TIME:' + str;
}



ICS.prototype.domain = function(domain) {
   "use strict";
   this.options.domain = domain;
   return this;
};

ICS.prototype.method = function(method) {
   "use strict";
   this.options.method = method;
   return this;
};

ICS.prototype.prodID = function(prodID) {
   "use strict";
   this.options.PRODID = prodID;
   return this;
};


ICS.prototype.addEvents = function(events) {
   "use strict"; 
   this.events = this.events.concat(events);
   return this;
};

ICS.prototype.x_tension = function(key, value) {
   "use strict";
   this.x_tensions[key] = value;
   return this;
};



//ICS.prototype._get_x_tension = function(key) {
//   "use strict";
//   var v = this.options[key];
//   if (v)
//      return key.replace(/_/g, '-') + ':' + _escape(v) + '\n';
//   else
//      return '';
//};



ICS.prototype._calProps = function(x_tensions) {
   "use strict";
   if (!this.options.PRODID)
      throw "PRODID is required";

   var calprops = 'BEGIN:VCALENDAR\nVERSION:2.0\n' +
      'PRODID:' + _escape(this.options.PRODID) + '\n' +
      'METHOD:' + _escape(this.options.method || 'PUBLISH') + '\n';

   for (var key in this.x_tensions) {
      var v = this.x_tensions[key];
      calprops += key.replace(/_/g, '-') + ':' + _escape(v) + '\n';
   }
   
   return calprops;
};



ICS.prototype.toICS = function(eventVals) {
   "use strict";
   var asICS = this._calProps();
   for (var i=0; i<this.events.length; i++) {
      asICS += this.eventToICS(this.events[i], eventVals);
   }
     
   return asICS + 'END:VCALENDAR\n';
};



ICS.prototype.eventToICS = function(event, moreEventVals) {
   "use strict";
   var allDay = event.allDay || this.options.allEventsAreAllDay;
   var uid = _escape(event.UID || event.uid);
   if (!uid)
      throw 'UID is required';  // add my own???
   var required = 'BEGIN:VEVENT\n' +
           'DTSTART;' + datestr(event.DTSTART || event.start, allDay) + '\n' +
           'DTEND;' + datestr(event.DTEND || event.end, allDay) + '\n' +
           'UID:' + uid + '@' + this.options.domain + '\n' +
           'DTSTAMP:' + datestr(event.DTSTAMP || event.stamp || new Date(), false, true) + '\n' +
           'CREATED:' + datestr(event.CREATED || new Date(), false, true) + '\n' +
           'SUMMARY:' + _escape(event.SUMMARY || event.summary) + '\n';
   
   var more = '';
   if (moreEventVals && moreEventVals.length) {
      for (var i=0; i<moreEventVals.length; i++)
         more += _optVal(event, moreEventVals[i].toUpperCase());
   }
    
   return required + more + 'END:VEVENT\n';   
};


module.exports = ICS;