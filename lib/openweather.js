/**
 * @author Morgan Conrad
 * Copyright(c) 2016
 * This software is released under the MIT license  (http://opensource.org/licenses/MIT)
 */


function OpenWeather(apiKey, options) {
   "use strict";

   // allow "static" usage, save user from misuse...
   if (!(this instanceof OpenWeather))
      return new OpenWeather(apiKey, options);

   this.apiKey = apiKey;
   this.where = '';
   this.more = '';
   this.options = assign({}, options);
}

/*
OpenWeather.DEFAULT_VALUES = {
   mode: 'json',
   count: 50,
   eventsToRead:    1000,         // an integer, 0 means "all events"
   maxPerLine:      10,           // only applies in byParam mode
   dataFormat:      'asString',   // alternatives are 'asNumber', 'asBoth', 'asNone'
   groupBy:         'byEvent'     // alternative is 'byParam'
};
*/

OpenWeather.prototype.byCityName = function(cityName) {
   this.where = '&q=' + cityName;
   return this;
};

OpenWeather.prototype.byIDs = function(ids) {
   var args = Array.prototype.slice.call(arguments);
   this.where = '&id=' + args.join(',');
   return this;
};

OpenWeather.prototype.byCoordinates = function(lat, lon) {
   this.where = '&lat=' + lat + "&lon" + lon;
   return this;
};

OpenWeather.prototype.byZIP = function(zip, country) {
   this.where = '&zip=' + zip + "," + country;
   return this;
};


OpenWeather.prototype.inRectangle = function(left, bottom, right, top) {
   var args = Array.prototype.slice.call(arguments);
   this.where = '&bbox=' + args.join(',');
   return this;
};

OpenWeather.prototype.inCircle = function(lat, lon, cnt) {
   this.byCoordinates(lat, lon);
   this.where = this.where + "&cnt=" + (cnt || 50);
   return this;
};


OpenWeather.prototype.current = function(callback) {
   this.getWeather('weather', callback);
};


OpenWeather.prototype.historical = function(type, start, end, cnt, callback) {
   type = type || 'hour';
   this.more = '&type=' + type + '&start' + dateToUnixTime(start);
   if (end)
      this.more = this.more + '&end' + dateToUnixTime(end);
   else
      this.more = this.more + '&cnt' + (cnt || 50);
   this.getWeather('history', callback);
};


OpenWeather.prototype.station = function(callback) {
   this.getWeather('station', callback);
};


OpenWeather.prototype.forecast = function(callback) {
   this.getWeather('forecast', callback);
};


OpenWeather.prototype.daily = function(callback) {
   this.getWeather('forecast/daily', callback);
};




OpenWeather.prototype.getWeather = function(path, callback) {
   var url = 'http://api.openweathermap.org/data/2.5' + path;
   url += "?APIID=" + this.apiKey + this.more + this.where;
   url += join(this.options);

   httpget(url, callback);
};



// much like Object.assign()
function assign(to, from /* ... */) {
   to = to || {};
   for (var i=1; i<arguments.length; i++) {
      var argument = arguments[i];
      if (argument) {
         Object.keys(argument).forEach(function (key) {
            to[key] = argument[key];
         });
      }
   }

   return to;
}


function join(object, delim) {
   delim = delim || ',';
   var result = '';
   Object.keys(object).forEach(function (key, idx) {
      if (idx)
         result += delim;
      result += (key + '=' + object[key]);
   });

   return result;
}


function dateToUnixTime(date) {
   if (utils.isDate(date))
      return date.getTime();
   if (utils.isNumber(date))
      return date;
   return new Date(date).getTime();
}


function httpget(url, callback) {
   http.get(url, function(res){
      res.setEncoding('utf-8');
      var body = '';
      res.on('data', function (chunk) {
         body += chunk;
      });
      res.on('error', function(err){
         return callback(err, null);
      });
      res.on('end', function(){
         res.body = body;
         res.parsed = {};
         try {  // TODO whatif they want XML???
            res.parsed = JSON.parse(body);
         }
         catch (e){
            res.parsed = {error:e}
         }

         return callback(null, res);
      });
   });
}

module.exports = OpenWeather;